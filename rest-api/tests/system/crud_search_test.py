import os

import math
import pytest


@pytest.fixture
def db():
    os.environ['POSTGRES_DB'] = 'bondanalyzer_v2_int'
    from database import SessionLocal
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def test__get_stocks_by_range_filter(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False
    )
    stocks = crud.get_stocks_by_range_filter(db, range_filter)
    assert len(stocks) == 13478


def test__get_stock_specifics_by_range_filter__no_real_filter(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 13478
    assert specifics.sectors == []
    assert math.isclose(specifics.market_cap_min, 0, rel_tol=1e-3)
    assert math.isclose(specifics.market_cap_max, 1867494546, rel_tol=1e-3)
    assert math.isclose(specifics.months_min, 0, rel_tol=1e-3)
    assert math.isclose(specifics.months_max, 120, rel_tol=1e-3)
    assert math.isclose(specifics.score_min, -26, rel_tol=1e-3)
    assert math.isclose(specifics.score_max, 92, rel_tol=1e-3)
    assert math.isclose(specifics.geopac_min, -0.966, rel_tol=1e-3)
    assert math.isclose(specifics.geopac_max, 1.449, rel_tol=1e-3)
    assert math.isclose(specifics.gain_consistency_min, 0, rel_tol=1e-3)
    assert math.isclose(specifics.gain_consistency_max, 0.901, rel_tol=1e-3)
    assert math.isclose(specifics.loss_ratio_min, 0, rel_tol=1e-3)
    assert math.isclose(specifics.loss_ratio_max, 38.5034, rel_tol=1e-3)
    assert math.isclose(specifics.trend_min, -99.220, rel_tol=1e-3)
    assert math.isclose(specifics.trend_max, 192.354, rel_tol=1e-3)


def test__get_stock_specifics_by_range_filter__market_cap(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False,
        market_cap_min=1e6,
        market_cap_max=1e10,
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 8
    assert specifics.sectors == []
    assert math.isclose(specifics.market_cap_min, 2214282, rel_tol=1e-3)
    assert math.isclose(specifics.market_cap_max, 1867494546, rel_tol=1e-3)


def test__get_stock_specifics_by_range_filter__months(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False,
        months_min=29,
        months_max=79
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 40
    assert math.isclose(specifics.months_min, 29, rel_tol=1e-3)
    assert math.isclose(specifics.months_max, 79, rel_tol=1e-3)


def test__get_stock_specifics_by_range_filter__score(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False,
        score_min=1,
        score_max=91
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 29
    assert math.isclose(specifics.score_min, 1, rel_tol=1e-3)
    assert math.isclose(specifics.score_max, 75, rel_tol=1e-3)


def test__get_stock_specifics_by_range_filter__geopac(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False,
        geopac_min=0.05,
        geopac_max=1.5
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 20
    assert math.isclose(specifics.geopac_min, 0.0522, rel_tol=1e-3)
    assert math.isclose(specifics.geopac_max, 1.449, rel_tol=1e-3)


def test__get_stock_specifics_by_range_filter__gain_consistency(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False,
        gain_consistency_min=0.01,
        gain_consistency_max=0.6
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 73
    assert math.isclose(specifics.gain_consistency_min, 0.0303, rel_tol=1e-3)
    assert math.isclose(specifics.gain_consistency_max, 0.581, rel_tol=1e-3)


def test__get_stock_specifics_by_range_filter__loss_ratio(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False,
        loss_ratio_min=0.01,
        loss_ratio_max=10
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 70
    assert math.isclose(specifics.loss_ratio_min, 2.201, rel_tol=1e-3)
    assert math.isclose(specifics.loss_ratio_max, 9.991, rel_tol=1e-3)


def test__get_stock_specifics_by_range_filter__trend(db):
    import crud
    from bondanalyzer_schemes.database_schemes import StockSearchRangeFilter
    range_filter = StockSearchRangeFilter(
        inner=False,
        trend_min=0.01,
        trend_max=20
    )
    specifics = crud.get_stock_specifics_by_range_filter(db, range_filter)
    assert specifics.count == 19
    assert math.isclose(specifics.trend_min, .1361, rel_tol=1e-3)
    assert math.isclose(specifics.trend_max, 17.6385, rel_tol=1e-3)
