import os

import pytest
from starlette.testclient import TestClient


@pytest.fixture
def tc():
    os.environ['POSTGRES_DB'] = 'bondanalyzer_v2_int'
    from main import app
    yield TestClient(app)


def test__get_stocks(tc):
    resp = tc.post('/api/v1/stock/search', json={'inner': 'False'})
    assert len(resp.json()) == 13478


def test__get_stock_specifics(tc):
    resp = tc.post('/api/v1/stock/search/specifics', json={'inner': 'False'})
    assert resp.json()['count'] == 13478
