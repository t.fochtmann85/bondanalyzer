import pytest
from starlette.testclient import TestClient


@pytest.fixture
def test_client():
    from main import app
    return TestClient(app)
