import os

import pytest
import time
from starlette.testclient import TestClient


@pytest.fixture
def tc():
    os.environ['POSTGRES_DB'] = 'bondanalyzer_v2_int'
    from main import app
    yield TestClient(app)


def test__get_signals(tc):
    start = time.time()
    tc.get('/api/v1/signal')
    duration = time.time() - start
    assert duration < 2.5


def test__get_stocks(tc):
    start = time.time()
    resp = tc.get('/api/v1/stock?inner=False')
    duration = time.time() - start
    assert duration < 2.5
