import datetime
import pytest
from httpx import Response
from starlette import status

from database import SessionLocal
from models import Signal, Stock


@pytest.fixture
def tc(test_client):
    yield test_client
    db = SessionLocal()
    db.query(Signal).delete()
    db.commit()
    db.query(Stock).delete()
    db.commit()


@pytest.fixture
def tc_with_apc(tc, stock_info_apc_create, auth_header):
    resp = tc.post('/api/v1/stock', json=stock_info_apc_create, headers=auth_header)
    assert resp.status_code == status.HTTP_201_CREATED
    yield tc


@pytest.fixture
def stock_info_apc_create() -> dict:
    return {
        "inner": True,
        "name": "Apple",
        "ticker": "APC.F",
        "isin": "US0378331005",
        "sector": "Technologie",
        "original": True,
        "currency": "EUR"
    }


@pytest.fixture
def stock_info_apc_read() -> dict:
    return {
        'currency': 'EUR',
        'gain_consistency': 0.0,
        'geopac': 0.0,
        'inner': True,
        'latest_ma_200': 0.0,
        'latest_price': 0.0,
        'loss_ratio': 0.0,
        'market_cap': 0,
        'months': 0,
        'name': 'Apple',
        'original': True,
        'sector': 'Technologie',
        "isin": "US0378331005",
        'ticker': 'APC.F',
        'trend': 0.0,
        'score': 0,
        'ath': 0.0,
        'ath_diff': 0.0,
    }


@pytest.fixture
def signal_info() -> dict:
    return {
        "date": datetime.date.today().strftime("%Y-%m-%d"),
        "signal_type": 1
    }


@pytest.fixture
def signal_info2() -> dict:
    return {
        "date": datetime.date(2023, 3, 4).strftime("%Y-%m-%d"),
        "signal_type": 4
    }


@pytest.fixture
def auth_header() -> dict:
    return {"Authorization": "Bearer 3095c3e4f1465133e5e6be134eb2ebe2"}


class TestGETstock:
    @pytest.mark.parametrize("inner", [
        pytest.param(True, id="inner"),
        pytest.param(False, id="foreign"),
    ])
    def test__get_stocks__empty(self, inner, tc):
        resp: Response = tc.get('/api/v1/stock', params={'inner': inner})
        assert resp.status_code == status.HTTP_200_OK
        assert resp.json() == []

    def test__get_stocks(self, stock_info_apc_read, stock_info_apc_create, tc_with_apc):
        resp: Response = tc_with_apc.get('/api/v1/stock', params={'inner': True})
        assert resp.status_code == status.HTTP_200_OK
        data = resp.json()
        data[0].pop('id')  # since it is incrementing
        assert data == [stock_info_apc_read]

        resp: Response = tc_with_apc.get('/api/v1/stock', params={'inner': False})
        assert resp.status_code == status.HTTP_200_OK
        assert resp.json() == []

    def test__get_stock__not_found(self, tc):
        resp = tc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_404_NOT_FOUND
        assert resp.json()['detail'] == 'Stock with ticker APC.F is unknown to the database.'

    @pytest.mark.parametrize("ticker", [
        pytest.param("APC.F", id="upper case"),
        pytest.param("apc.f", id="lower case"),
    ])
    def test_get_stock(self, ticker, stock_info_apc_read, tc_with_apc):
        resp = tc_with_apc.get(f'/api/v1/stock/{ticker}')
        assert resp.status_code == status.HTTP_200_OK
        data = resp.json()
        data.pop('id')  # since it is incrementing
        assert data == stock_info_apc_read


class TestPUTstock:
    def test__update_stock(self, stock_info_apc_read, tc_with_apc, auth_header):
        resp = tc_with_apc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_200_OK
        stock_read = resp.json()
        stock_read.pop('id')  # since it is incrementing
        assert stock_read == stock_info_apc_read

        changes = {
            'inner': False,
            'name': 'Opel',
            'sector': 'Automobil',
            'original': False,
            'currency': 'CHF',
            'market_cap': 42,
            'geopac': 0.25,
            'gain_consistency': 0.95,
            'loss_ratio': 1.0,
            'latest_price': 144.2,
            'latest_ma_200': 130.1,
            'months': 1234,
            "isin": "DE000A1EWWW0",
            'ath': 111.24
        }

        for key, value in changes.items():
            assert stock_read[key] != value
            resp = tc_with_apc.put('/api/v1/stock/APC.F', json={key: value}, headers=auth_header)
            assert resp.status_code == status.HTTP_200_OK
            stock_read = resp.json()
            assert stock_read[key] == value

    def test__update_stock__not_found(self, stock_info_apc_read, tc, auth_header):
        resp = tc.put('/api/v1/stock/APC.F', json={'inner': False}, headers=auth_header)
        assert resp.status_code == status.HTTP_404_NOT_FOUND
        assert resp.json()['detail'] == 'Stock with ticker APC.F is unknown to the database.'

    def test_open_api__update_stock(self, auth_header, tc_with_apc):
        resp = tc_with_apc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_200_OK

        resp = tc_with_apc.put('/api/v1/stock/APC.F', json=[{'outer': False}], headers=auth_header)
        assert resp.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.parametrize("headers, status_code", [
        pytest.param({}, status.HTTP_403_FORBIDDEN, id="no auth"),
        pytest.param({"Authorization": "Bearer 3095c3e4f"}, status.HTTP_401_UNAUTHORIZED, id="wrong auth"),
    ])
    def test_auth__update_stock(self, headers, status_code, stock_info_apc_create, tc_with_apc):
        resp = tc_with_apc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_200_OK

        resp = tc_with_apc.put('/api/v1/stock/APC.F', json={'inner': False}, headers=headers)
        assert resp.status_code == status_code


class TestPOSTstock:
    def test__create_stock__twice_the_same(self, stock_info_apc_read, stock_info_apc_create, auth_header, tc):
        stock_info_apc_create['ticker'] = stock_info_apc_create['ticker'].lower()
        resp = tc.post('/api/v1/stock', json=stock_info_apc_create, headers=auth_header)
        assert resp.status_code == status.HTTP_201_CREATED
        data = resp.json()
        data.pop('id')  # since it is incrementing
        assert data == stock_info_apc_read

        # same again
        resp = tc.post('/api/v1/stock', json=stock_info_apc_create, headers=auth_header)
        assert resp.status_code == status.HTTP_409_CONFLICT
        assert resp.json()['detail'] == 'Stock with ticker APC.F already exists.'

    def test_open_api__create_stock(self, auth_header, tc):
        stock_info = {
            "ticker": "APC.F",
        }

        resp = tc.post('/api/v1/stock', json=stock_info, headers=auth_header)
        assert resp.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.parametrize("headers, status_code", [
        pytest.param({}, status.HTTP_403_FORBIDDEN, id="no auth"),
        pytest.param({"Authorization": "Bearer 3095c3e4f"}, status.HTTP_401_UNAUTHORIZED, id="wrong auth"),
    ])
    def test_auth__create_stock(self, headers, status_code, stock_info_apc_create, tc):
        resp = tc.post('/api/v1/stock', json=stock_info_apc_create, headers=headers)
        assert resp.status_code == status_code


class TestPOSTstockFilter:
    def test__get_stocks_filter(self, stock_info_apc_read, tc_with_apc):
        filter_json = {
            "isin": "US0378331005",
            "inner": True,
            "ticker": "APC.F"
        }
        resp: Response = tc_with_apc.post('/api/v1/stock/filter', json=filter_json)
        assert resp.status_code == status.HTTP_200_OK
        data = resp.json()
        data[0].pop('id')  # since it is incrementing
        assert data == [stock_info_apc_read]

        filter_json2 = {
            "isin": "US0378331005",
            "inner": True,
            "ticker": "APC"
        }

        resp: Response = tc_with_apc.post('/api/v1/stock/filter', json=filter_json2)
        assert resp.status_code == status.HTTP_200_OK
        assert resp.json() == []


class TestDELETEstock:
    def test__delete_stock(self, tc_with_apc, auth_header):
        resp = tc_with_apc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_200_OK

        resp = tc_with_apc.delete('/api/v1/stock/APC.F', headers=auth_header)
        assert resp.status_code == status.HTTP_204_NO_CONTENT

        resp = tc_with_apc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_404_NOT_FOUND

    def test__delete_stock__not_found(self, stock_info_apc_read, tc, auth_header):
        resp = tc.delete('/api/v1/stock/APC.F', headers=auth_header)
        assert resp.status_code == status.HTTP_404_NOT_FOUND
        assert resp.json()['detail'] == 'Stock with ticker APC.F is unknown to the database.'

    @pytest.mark.parametrize("headers, status_code", [
        pytest.param({}, status.HTTP_403_FORBIDDEN, id="no auth"),
        pytest.param({"Authorization": "Bearer 3095c3e4f"}, status.HTTP_401_UNAUTHORIZED, id="wrong auth"),
    ])
    def test_auth__delete_stock(self, headers, status_code, stock_info_apc_create, tc_with_apc):
        resp = tc_with_apc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_200_OK

        resp = tc_with_apc.delete('/api/v1/stock/APC.F', headers=headers)
        assert resp.status_code == status_code


class TestPOSTstockSignal:
    def test__create_stock_signal(self, tc_with_apc, signal_info, auth_header):
        resp = tc_with_apc.get('/api/v1/stock/APC.F')
        assert resp.status_code == status.HTTP_200_OK

        resp = tc_with_apc.post('/api/v1/stock/APC.F/signal', json=signal_info, headers=auth_header)
        assert resp.status_code == status.HTTP_201_CREATED
        assert resp.json() == signal_info

        # already added
        resp = tc_with_apc.post('/api/v1/stock/APC.F/signal', json=signal_info, headers=auth_header)
        assert resp.status_code == status.HTTP_204_NO_CONTENT
        assert resp.text == ""

    def test__create_stock_signal__not_found(self, tc, signal_info, auth_header):
        resp = tc.post('/api/v1/stock/APC.F/signal', json=signal_info, headers=auth_header)
        assert resp.status_code == status.HTTP_404_NOT_FOUND
        assert resp.json()['detail'] == 'Stock with ticker APC.F is unknown to the database.'

    def test_open_api__create_stock_signal(self, auth_header, tc_with_apc):
        resp = tc_with_apc.post('/api/v1/stock/APC.F/signal', json={"signal_type": 99}, headers=auth_header)
        assert resp.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.parametrize("headers, status_code", [
        pytest.param({}, status.HTTP_403_FORBIDDEN, id="no auth"),
        pytest.param({"Authorization": "Bearer 3095c3e4f"}, status.HTTP_401_UNAUTHORIZED, id="wrong auth"),
    ])
    def test_auth__create_stock_signal(self, headers, status_code, signal_info, tc):
        resp = tc.post('/api/v1/stock/APC.F/signal', json=signal_info, headers=headers)
        assert resp.status_code == status_code


class TestGETsignal:
    def test__get_signals__none(self, tc):
        resp = tc.get('/api/v1/signal')
        assert resp.status_code == status.HTTP_200_OK
        assert resp.json() == []

    def test__get_signals__one(self, tc_with_apc, auth_header, signal_info, signal_info2, stock_info_apc_read):
        resp = tc_with_apc.post('/api/v1/stock/APC.F/signal', json=signal_info, headers=auth_header)
        assert resp.status_code == status.HTTP_201_CREATED
        assert resp.json() == signal_info
        resp = tc_with_apc.post('/api/v1/stock/APC.F/signal', json=signal_info2, headers=auth_header)
        assert resp.status_code == status.HTTP_201_CREATED
        assert resp.json() == signal_info2

        resp = tc_with_apc.get('/api/v1/signal')
        assert resp.status_code == status.HTTP_200_OK, resp.text
        data = resp.json()
        assert len(data) == 1

        stock_signal = data[0]
        stock_signal['stock'].pop('id')
        stock_signal['signal'].pop('id')
        stock_signal['signal'].pop('stock_id')
        assert data == [{
            "stock": stock_info_apc_read,
            "signal": signal_info
        }]
