import config


def test__get_api_keys():
    assert len(config.get_api_keys()) == 1
