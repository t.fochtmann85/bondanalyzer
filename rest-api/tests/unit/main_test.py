
import pytest
from fastapi import HTTPException
from mockito import when, mock, verify

import main


@pytest.fixture
def sender():
    return mock({'send': lambda: None})


def raises():
    d = dict()
    return d["a"]


def test__fetch_internal_exception(sender):
    with (
        when(main).Sender(...).thenReturn(sender)
    ):
        with pytest.raises(HTTPException):
            main._fetch_internal_exception(raises)

        verify(main).Sender(
            "[Bondanalyzer] Internal error - KeyError: 'a'",
            'Traceback (most recent call last):\n'
            '  File "C:\\Users\\tobst\\PycharmProjects\\bondanalyzer\\rest-api\\rest_api\\main.py", line 161, in _fetch_internal_exception\n'
            '    return func(*args, **kwargs)\n'
            '  File "C:\\Users\\tobst\\PycharmProjects\\bondanalyzer\\rest-api\\tests\\unit\\main_test.py", line 16, in raises\n'
            '    return d["a"]\nKeyError: \'a\'\n',
            ["hubba@bubba.net"]
        )
        verify(sender).send()
