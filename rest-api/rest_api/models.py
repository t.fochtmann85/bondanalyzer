from sqlalchemy import BigInteger, Boolean, Column, Date, Enum, Float, ForeignKey, func, Integer, String, \
    UniqueConstraint, case
from sqlalchemy.ext.hybrid import hybrid_property

from bondanalyzer_schemes.database_schemes import SignalType
from database import Base


class Stock(Base):
    __tablename__ = "stock"

    id = Column(Integer, primary_key=True)
    inner = Column(Boolean, nullable=False, default=False)
    name = Column(String, nullable=False)
    ticker = Column(String, nullable=False, unique=True)
    isin = Column(String, nullable=False, unique=True)
    sector = Column(String)
    original = Column(Boolean, nullable=False, default=False)
    currency = Column(String, nullable=False)
    market_cap = Column(BigInteger, nullable=False, default=0)
    geopac = Column(Float(precision=2), nullable=False, default=0)
    gain_consistency = Column(Float(precision=2), nullable=False, default=0)
    loss_ratio = Column(Float(precision=2), nullable=False, default=0)
    latest_price = Column(Float(precision=2), nullable=False, default=0)
    latest_ma_200 = Column(Float(precision=2), nullable=False, default=0)
    ath = Column(Float(precision=2), nullable=False, default=0)
    months = Column(Integer, nullable=False, default=0)

    @hybrid_property
    def exchange_symbol(self) -> str:
        return func.split_part(self.ticker, '.', 2)

    @hybrid_property
    def score(self) -> float:
        if self.loss_ratio != .0:
            return self.geopac * self.gain_consistency * 1000 / self.loss_ratio
        return .0

    @score.expression
    def score(cls):
        return case(
            (cls.loss_ratio == 0, 0),
            else_=cls.geopac * cls.gain_consistency * 1000 / cls.loss_ratio
        )

    @hybrid_property
    def trend(self) -> float:
        if self.latest_ma_200 != .0:
            return (self.latest_price / self.latest_ma_200 - 1) * 100
        return .0

    @trend.expression
    def trend(cls):
        return case(
            (cls.latest_ma_200 == 0, 0),
            else_=(cls.latest_price / cls.latest_ma_200 - 1) * 100
        )

    @hybrid_property
    def ath_diff(self) -> float:
        if self.ath != .0:
            return (self.latest_price / self.ath - 1) * 100
        return .0

    @ath_diff.expression
    def ath_diff(cls):
        return case(
            (cls.ath == 0, 0),
            else_=(cls.latest_price / cls.ath - 1) * 100
        )


class Signal(Base):
    __tablename__ = "signal"

    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False)
    stock_id = Column(Integer, ForeignKey('stock.id'), nullable=False)
    signal_type = Column(Enum(SignalType), nullable=False)

    __table_args__ = (
        UniqueConstraint('date', 'stock_id', 'signal_type'),
    )
