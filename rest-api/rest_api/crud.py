import math
from sqlalchemy import func
from sqlalchemy.orm import Session

from bondanalyzer_schemes.database_schemes import SignalCreate, StockCreate, StockUpdate, StockFilter, \
    StockSearchRangeFilter, \
    StockSearchResultSpecifics
from models import Signal, Stock


def create_stock(db: Session, stock: StockCreate) -> Stock:
    db_stock = Stock(**stock.dict())
    _add_commit_refresh(db, db_stock)
    return db_stock


def get_stocks_by_exact_filter(db: Session, stock_filter: StockFilter) -> list[Stock]:
    filter_args = list()
    for field_name in stock_filter.__fields__.keys():
        filter_value = getattr(stock_filter, field_name)
        if filter_value is not None:
            filter_args.append(
                getattr(Stock, field_name) == filter_value
            )

    db_stocks = db.query(Stock).filter(*filter_args).all()
    return db_stocks


def get_stocks_by_range_filter(db: Session, search_range: StockSearchRangeFilter) -> list[Stock]:
    return db.query(Stock).filter(*_build_filter_by_search_range(search_range)).all()


def get_stock_specifics_by_range_filter(db: Session,
                                        search_range: StockSearchRangeFilter) -> StockSearchResultSpecifics:
    filter_args = _build_filter_by_search_range(search_range)
    specifics = db.query(
        func.count(Stock.id),
        func.min(Stock.market_cap),
        func.max(Stock.market_cap),
        func.min(Stock.months),
        func.max(Stock.months),
        func.min(Stock.score),
        func.max(Stock.score),
        func.min(Stock.geopac),
        func.max(Stock.geopac),
        func.min(Stock.gain_consistency),
        func.max(Stock.gain_consistency),
        func.min(Stock.loss_ratio),
        func.max(Stock.loss_ratio),
        func.min(Stock.trend),
        func.max(Stock.trend),
    ).filter(*filter_args).first()
    sectors = db.query(Stock.sector).filter(*filter_args).group_by(Stock.sector).order_by(Stock.sector).all()
    sectors = [elem for elem, in sectors]
    sectors = list(filter(lambda elem: elem is not None, sectors))
    return StockSearchResultSpecifics(
        count=specifics[0],
        sectors=sectors,
        market_cap_min=specifics[1],
        market_cap_max=specifics[2],
        months_min=specifics[3],
        months_max=specifics[4],
        score_min=math.floor(specifics[5]),
        score_max=math.ceil(specifics[6]),
        geopac_min=specifics[7],
        geopac_max=specifics[8],
        gain_consistency_min=specifics[9],
        gain_consistency_max=specifics[10],
        loss_ratio_min=specifics[11],
        loss_ratio_max=specifics[12],
        trend_min=specifics[13],
        trend_max=specifics[14],
    )


def _build_filter_by_search_range(search_range: StockSearchRangeFilter) -> list:
    filter_args = list()

    if search_range.inner is not None:
        filter_args.append(Stock.inner.is_(search_range.inner))

    if search_range.sector is not None:
        filter_args.append(Stock.sector == search_range.sector)

    if search_range.market_cap_min is not None:
        filter_args.append(Stock.market_cap >= search_range.market_cap_min)

    if search_range.market_cap_max is not None:
        filter_args.append(Stock.market_cap <= search_range.market_cap_max)

    if search_range.months_min is not None:
        filter_args.append(Stock.months >= search_range.months_min)

    if search_range.months_max is not None:
        filter_args.append(Stock.months <= search_range.months_max)

    if search_range.score_min is not None:
        filter_args.append(Stock.score >= search_range.score_min)

    if search_range.score_max is not None:
        filter_args.append(Stock.score <= search_range.score_max)

    if search_range.geopac_min is not None:
        filter_args.append(Stock.geopac >= search_range.geopac_min)

    if search_range.geopac_max is not None:
        filter_args.append(Stock.geopac <= search_range.geopac_max)

    if search_range.gain_consistency_min is not None:
        filter_args.append(Stock.gain_consistency >= search_range.gain_consistency_min)

    if search_range.gain_consistency_max is not None:
        filter_args.append(Stock.gain_consistency <= search_range.gain_consistency_max)

    if search_range.loss_ratio_min is not None:
        filter_args.append(Stock.loss_ratio >= search_range.loss_ratio_min)

    if search_range.loss_ratio_max is not None:
        filter_args.append(Stock.loss_ratio <= search_range.loss_ratio_max)

    if search_range.trend_min is not None:
        filter_args.append(Stock.trend >= search_range.trend_min)

    if search_range.trend_max is not None:
        filter_args.append(Stock.trend <= search_range.trend_max)

    return filter_args


def get_stock(db: Session, ticker: str) -> Stock | None:
    return _get_stock_by_ticker(db, ticker)


def update_stock(db: Session, ticker: str, stock_update: StockUpdate) -> Stock:
    db_stock = _get_stock_by_ticker(db, ticker)

    for field_name in stock_update.__fields__.keys():
        new_value = getattr(stock_update, field_name)
        if new_value is not None:
            setattr(db_stock, field_name, new_value)

    _add_commit_refresh(db, db_stock)
    return db_stock


def delete_stock(db: Session, ticker: str) -> None:
    db_stock = _get_stock_by_ticker(db, ticker)
    _delete_commit(db, db_stock)


def has_stock_by_ticker(db: Session, ticker: str) -> bool:
    return _get_stock_by_ticker(db, ticker) is not None


def create_stock_signal(db: Session, ticker: str, signal: SignalCreate) -> Signal | None:
    db_stock = _get_stock_by_ticker(db, ticker)

    db_signal = db.query(Signal).filter(Signal.stock_id == db_stock.id, Signal.date == signal.date,
                                        Signal.signal_type == signal.signal_type).one_or_none()
    if db_signal is None:
        db_signal = Signal(**signal.dict(), stock_id=db_stock.id)
        _add_commit_refresh(db, db_signal)
        return db_signal
    else:
        return None


def get_stock_signals(db: Session, ticker: str, limit: int = 1) -> list[Signal]:
    db_stock = _get_stock_by_ticker(db, ticker)
    return db.query(Signal).filter(Signal.stock_id == db_stock.id).order_by(Signal.date.desc()).limit(limit).all()


def get_signals(db: Session) -> list[tuple[Stock, Signal]]:
    # no group by possible with postgreSQL :(
    for stock in db.query(Stock).filter(Stock.inner.is_(True)).all():
        most_recent_signal = db.query(Signal).filter(stock.id == Signal.stock_id).order_by(Signal.date.desc()).first()
        if most_recent_signal:
            yield stock, most_recent_signal


def _get_stock_by_ticker(db: Session, ticker: str) -> Stock | None:
    return db.query(Stock).filter(func.upper(Stock.ticker) == func.upper(ticker)).one_or_none()


def _add_commit_refresh(db: Session, obj):
    db.add(obj)
    db.commit()
    db.refresh(obj)


def _delete_commit(db: Session, obj):
    db.delete(obj)
    db.commit()
