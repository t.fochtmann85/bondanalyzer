import logging
import logging.config
import os
import traceback
from pathlib import Path
from typing import Callable, Any

import uvicorn
from dotenv import load_dotenv
from fastapi import Depends, FastAPI, status, HTTPException, Response
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from sqlalchemy.orm import Session
from starlette.middleware.cors import CORSMiddleware

from mail_sender.sender import Sender

load_dotenv()
logging.config.fileConfig(Path(__file__).parent / "logging.ini")

import config
import crud
import models
from bondanalyzer_schemes.database_schemes import SignalCreate, SignalRead, StockCreate, StockUpdate, StockRead, \
    StockFilter, StockSearchRangeFilter, StockSearchResultSpecifics
from bondanalyzer_schemes.mixin import StockSignal
from database import engine, SessionLocal

models.Base.metadata.create_all(bind=engine)
bearer_scheme = HTTPBearer()  # use token authentication
app = FastAPI(
    title="Bondanalyzer v2 REST-API",
    version="0.1.0",
    debug=True,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

apiv1 = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def api_key_auth(auth_cred: HTTPAuthorizationCredentials = Depends(bearer_scheme)):
    if auth_cred.credentials not in config.get_api_keys():
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Forbidden"
        )


@apiv1.get('/stock', response_model=list[StockRead], status_code=status.HTTP_200_OK)
def get_stocks(inner: bool = True, db: Session = Depends(get_db)):
    stock_filter = StockFilter(inner=inner)
    return crud.get_stocks_by_exact_filter(db, stock_filter)


@apiv1.post('/stock/filter', response_model=list[StockRead], status_code=status.HTTP_200_OK)
def get_stocks_by_exact_filter(stock_filter: StockFilter, db: Session = Depends(get_db)):
    return crud.get_stocks_by_exact_filter(db, stock_filter)


@apiv1.post('/stock/search', response_model=list[StockRead], status_code=status.HTTP_200_OK)
def get_stocks_by_range_filter(stock_search: StockSearchRangeFilter, db: Session = Depends(get_db)):
    return crud.get_stocks_by_range_filter(db, stock_search)


@apiv1.post('/stock/search/specifics', response_model=StockSearchResultSpecifics, status_code=status.HTTP_200_OK)
def get_stock_specifics_by_range_filter(stock_search: StockSearchRangeFilter, db: Session = Depends(get_db)):
    return crud.get_stock_specifics_by_range_filter(db, stock_search)


@apiv1.post('/stock', response_model=StockRead, status_code=status.HTTP_201_CREATED,
            dependencies=[Depends(api_key_auth)])
def create_stock(stock: StockCreate, db: Session = Depends(get_db)):
    if crud.has_stock_by_ticker(db, stock.ticker):
        raise HTTPException(
            detail=f'Stock with ticker {stock.ticker} already exists.',
            status_code=status.HTTP_409_CONFLICT
        )

    return _fetch_internal_exception(crud.create_stock, db, stock)


@apiv1.get('/stock/{ticker}', response_model=StockRead, status_code=status.HTTP_200_OK)
def get_stock(ticker: str, db: Session = Depends(get_db)):
    _assure_stock_presence_by_ticker(db, ticker)

    return _fetch_internal_exception(crud.get_stock, db, ticker)


@apiv1.put('/stock/{ticker}', response_model=StockRead, status_code=status.HTTP_200_OK,
           dependencies=[Depends(api_key_auth)])
def update_stock(ticker: str, stock_update: StockUpdate, db: Session = Depends(get_db)):
    _assure_stock_presence_by_ticker(db, ticker)

    return _fetch_internal_exception(crud.update_stock, db, ticker, stock_update)


@apiv1.delete('/stock/{ticker}', dependencies=[Depends(api_key_auth)], status_code=status.HTTP_204_NO_CONTENT)
def delete_stock(ticker: str, db: Session = Depends(get_db)):
    _assure_stock_presence_by_ticker(db, ticker)

    return _fetch_internal_exception(crud.delete_stock, db, ticker)


@apiv1.post('/stock/{ticker}/signal', response_model=SignalRead | None, status_code=status.HTTP_201_CREATED,
            dependencies=[Depends(api_key_auth)])
def create_stock_signal(ticker: str, signal: SignalCreate, response: Response, db: Session = Depends(get_db)):
    _assure_stock_presence_by_ticker(db, ticker)

    return _fetch_internal_exception(_create_stock_signal_if_not_exist, db, response, signal, ticker)


def _create_stock_signal_if_not_exist(db, response, signal, ticker) -> StockSignal | None:
    stock_signal = crud.create_stock_signal(db, ticker, signal)
    if stock_signal is None:
        response.status_code = 204
        return None
    else:
        return stock_signal


@apiv1.get('/stock/{ticker}/signal', response_model=list[SignalRead], status_code=status.HTTP_200_OK)
def get_stock_signals(ticker: str, limit: int = 1, db: Session = Depends(get_db)):
    _assure_stock_presence_by_ticker(db, ticker)

    return _fetch_internal_exception(crud.get_stock_signals, db, ticker, limit)


@apiv1.get('/signal', response_model=list[StockSignal], status_code=status.HTTP_200_OK)
def get_signals(db: Session = Depends(get_db)):
    return _fetch_internal_exception(_get_stock_signals, db)


def _get_stock_signals(db) -> list[StockSignal]:
    return [StockSignal(stock=st, signal=si) for st, si in crud.get_signals(db)]


def _assure_stock_presence_by_ticker(db: Session, ticker: str) -> None:
    if not crud.has_stock_by_ticker(db, ticker):
        raise HTTPException(
            detail=f'Stock with ticker {ticker} is unknown to the database.',
            status_code=status.HTTP_404_NOT_FOUND
        )


def _fetch_internal_exception(func: Callable, *args, **kwargs) -> Any:
    try:
        return func(*args, **kwargs)
    except Exception as exc:
        logging.exception(exc)
        _notify_admin(exc)
        raise HTTPException(
            detail=f'An error occurred:\n{exc}\n\nThe admin has been informed',
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
        )


def _notify_admin(exception: Exception) -> None:
    sender = Sender(
        f"[Bondanalyzer] Internal error - {exception.__class__.__name__}: {exception}",
        traceback.format_exc(),
        [os.environ['BONDANALYZER_ADMIN']]
    )
    sender.send()


def _check_on_startup():
    config.get_api_keys()


_check_on_startup()

app.mount("/api/v1", apiv1)

if __name__ == "__main__":
    uvicorn.run(app, port=8000)
