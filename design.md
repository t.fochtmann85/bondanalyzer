# Tasks

## Task1 - update prices

Runs each working day after closing of the german stock markets and add new prices to the database using the REST-API.

### Design

1. Request stocks to update along with the date to use as start from bondanalyzer-REST-API `GET` `/api/v1/stock/update`
2. Collects and prepares data from yahoo finance using the provides information
3. Pushes the new price data as a request to bondanalyzer-REST-API `POST` `/api/v1/stock/update`
4. Get all stocks
5. For each stock
   1. Get prices from REST-API
   2. Calculate metrics
   3. Calculate signals
6. Calculate metric ranking
7. for each stock
   1. Update metrics and signals via REST-API

## Task 2 - send signal mail

Sends the mail to recipients with the signals of the last day

### Design

1. Request signal information `GET` `/api/v1/stock/signals`
2. Build excel file containing signals
3. Send mail to recipients

## Task 3 - update foreign stocks

Runs every saturday and promotes new stocks to the foreign stock table.

### Design

1. Update information of (german) exchanges
2. Update information which stocks are traded on those exchanges
3. Request all foreign stock tickers known to the database `GET` `/api/v1/foreign_stock?info=ticker`
4. Request all stock tickers known to the database
5. Get all stocks traded on (german) exchanges and consolidate them (Frankfurt 1st, ...)
6. Update foreign stock information via REST-API (remove, keep & add)
7. Request all foreign stock tickers known to the database 
8. For each foreign stock:
   1. Get price data
   2. Calculate metrics
9. Update foreign stock metrics via REST-API

## Task 4 - send monthly update mail

On first of every month, new score data is calculated.

### Design

1. Request stock information `GET` `/api/v1/extended_stock`
2. Build excel file containing detailed stock information


