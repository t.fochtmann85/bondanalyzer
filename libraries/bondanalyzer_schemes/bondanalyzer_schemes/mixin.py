from pydantic import BaseModel

from .database_schemes import Signal, StockRead


class StockSignal(BaseModel):
    stock: StockRead
    signal: Signal
