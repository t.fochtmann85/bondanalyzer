import datetime
import enum
import math

from pydantic import BaseModel, constr, validator


class StockBase(BaseModel):
    inner: bool = False
    original: bool = False
    name: str
    currency: str
    ticker: constr(to_upper=True)
    isin: constr(to_upper=True)
    sector: str = None


class StockCreate(StockBase):
    pass


class StockUpdate(BaseModel):
    inner: bool | None = None
    name: str | None = None
    sector: str | None = None
    ticker: constr(to_upper=True) | None = None
    isin: constr(to_upper=True) | None = None
    original: bool | None = None
    currency: str | None = None
    market_cap: int | None = None
    geopac: float | None = None
    gain_consistency: float | None = None
    loss_ratio: float | None = None
    latest_price: float | None = None
    latest_ma_200: float | None = None
    months: int | None = None
    ath: float | None = None


class StockFilter(StockUpdate):
    id: int | None = None


class StockSearchRangeFilter(BaseModel):
    inner: bool | None = None
    sector: str | None = None
    market_cap_min: int | None = None
    market_cap_max: int | None = None
    months_min: int | None = None
    months_max: int | None = None
    score_min: int | None = None
    score_max: int | None = None
    geopac_min: float | None = None
    geopac_max: float | None = None
    gain_consistency_min: float | None = None
    gain_consistency_max: float | None = None
    loss_ratio_min: float | None = None
    loss_ratio_max: float | None = None
    trend_min: float | None = None
    trend_max: float | None = None


class StockSearchResultSpecifics(BaseModel):
    count: int
    sectors: list[str]
    market_cap_min: int
    market_cap_max: int
    months_min: int
    months_max: int
    score_min: int
    score_max: int
    geopac_min: float
    geopac_max: float
    gain_consistency_min: float
    gain_consistency_max: float
    loss_ratio_min: float
    loss_ratio_max: float
    trend_min: float
    trend_max: float

    @validator('geopac_min', 'gain_consistency_min')
    def floor3(cls, v):
        return floor_round(v, 3)

    @validator('geopac_max', 'gain_consistency_max')
    def ceil3(cls, v):
        return ceil_round(v, 3)

    @validator('loss_ratio_min', 'trend_min')
    def floor1(cls, v):
        return floor_round(v, 1)

    @validator('loss_ratio_max', 'trend_max')
    def ceil1(cls, v):
        return ceil_round(v, 1)


class StockRead(StockBase):
    id: int
    market_cap: int = 0
    geopac: float = .0
    gain_consistency: float = .0
    loss_ratio: float = .0
    latest_price: float = .0
    latest_ma_200: float = .0
    ath: float = .0
    months: int = 0
    score: int = 0
    trend: float = .0
    ath_diff: float = .0

    @validator('geopac', 'gain_consistency')
    def precision_check3(cls, v):
        return round(v, 3)

    @validator('loss_ratio', 'trend')
    def precision_check1(cls, v):
        return round(v, 1)

    class Config:
        orm_mode = True


class SignalType(enum.Enum):
    BUY = 1
    HOLD = 2
    BUYAGAIN = 3
    TRENDREVERSAL = 4


class SignalBase(BaseModel):
    date: datetime.date
    signal_type: SignalType


class SignalCreate(SignalBase):
    pass


class SignalRead(SignalBase):
    pass

    class Config:
        orm_mode = True


class Signal(SignalBase):
    id: int
    stock_id: int

    class Config:
        orm_mode = True


def floor_round(value: float, precision: int) -> float:
    return math.floor(value * 10 ** precision) / 10 ** precision


def ceil_round(value: float, precision: int) -> float:
    return math.ceil(value * 10 ** precision) / 10 ** precision
