import logging
import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path

from pydantic import BaseSettings

logger = logging.getLogger(__name__)


class MailSettings(BaseSettings):
    MAIL_SERVER: str
    MAIL_PORT: int
    MAIL_USERNAME: str
    MAIL_PASSWORD: str


class Sender:
    def __init__(self, subject: str, body: str, recipients: list[str]):
        self._subject = subject
        self._body = body
        self._recipients = recipients
        self.__mail_settings = MailSettings()

        self._message = None

    @property
    def message(self) -> MIMEMultipart:
        if self._message is None:
            message = MIMEMultipart()
            message["From"] = self.__mail_settings.MAIL_USERNAME
            message["To"] = ";".join(self._recipients)
            message["Subject"] = self._subject

            # Add body to email
            message.attach(MIMEText(self._body, "plain"))
            self._message = message
            logger.debug(f"Instantiated new message with subject {self._subject}.")
        return self._message

    def add_attachments(self, path: Path, main_type: str, sub_type: str, headers: dict[str, str]) -> None:
        with path.open(mode="rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase(main_type, sub_type)
            part.set_payload(attachment.read())

        # Encode file in ASCII characters to send by email
        encoders.encode_base64(part)

        # Add header as key/value pair to attachment part
        for key, value in headers.items():
            part.add_header(key, value)

        # Add attachment to message and convert message to string
        self.message.attach(part)
        logger.info(f"Attachment {path.name} successfully added to message.")

    def send(self) -> None:
        # Create a secure SSL context
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(self.__mail_settings.MAIL_SERVER,
                              self.__mail_settings.MAIL_PORT,
                              context=context) as server:
            server.login(self.__mail_settings.MAIL_USERNAME, self.__mail_settings.MAIL_PASSWORD)
            server.sendmail(
                self.__mail_settings.MAIL_USERNAME,
                self._recipients,
                self.message.as_string()
            )
            logger.info(f"Mail successfully sent.")
