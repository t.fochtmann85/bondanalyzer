import importlib.resources
import json

import pytest
from mockito import mock, expect, when, verify

import consolidate
from bondanalyzer_schemes.database_schemes import StockRead
from systems.alpha_vantage.alpha_vantage_client_dummy import AlphaVantageClientDummy
from systems.bondanalyzer_api_client.bondanalyzer_api_client_wrapper_dummy import BondanalyzerApiClientWrapperDummy
from systems.twelve_data_client.twelve_data_client_dummy import TwelvedataClientDummy
from systems.twelve_data_client.twelve_data_client_wrapper import TwelvedataClientWrapper
from systems.yfinance_client.yfinance_client_dummy import YfinanceClientDummy
from updater import UpdaterOfForeignStocks, UpdaterOfInnerStocks


@pytest.fixture
def bondanalyzer_outer_stock_dicts():
    with importlib.resources.path("resources", "bondanalyzer_foreign_stocks.json") as path:
        with path.open(encoding="utf-8") as f:
            return json.load(f)


@pytest.fixture
def bondanalyzer_inner_stock_dicts():
    with importlib.resources.path("resources", "bondanalyzer_stocks.json") as path:
        with path.open(encoding="utf-8") as f:
            return json.load(f)


class TestUpdaterOfForeignStocks:
    @pytest.fixture
    def bondanalyzer_wrapper(self, bondanalyzer_outer_stock_dicts):
        return mock({
            'get_stocks': lambda *a, **b: [StockRead(**elem) for elem in bondanalyzer_outer_stock_dicts]
        })

    @pytest.fixture
    def foreign_stocks_updater(self, bondanalyzer_wrapper) -> UpdaterOfForeignStocks:
        return UpdaterOfForeignStocks(
            TwelvedataClientWrapper(TwelvedataClientDummy()),
            bondanalyzer_wrapper,
            mock({}),
            mock({}),
        )

    def test__process(self, foreign_stocks_updater, bondanalyzer_wrapper):
        with (
            expect(consolidate.StockConsolidator).reduce_twelve_data_stocks_by_bondanalyzer_stocks(...).thenReturn([]),
            when(bondanalyzer_wrapper).delete_stock(...)
        ):
            foreign_stocks_updater._prepare_data_base()

            verify(bondanalyzer_wrapper, times=9636).delete_stock(...)


class TestUpdaterOfInnerStocks:
    @pytest.fixture
    def stocks_updater(self) -> UpdaterOfInnerStocks:
        return UpdaterOfInnerStocks(
            BondanalyzerApiClientWrapperDummy(),
            AlphaVantageClientDummy(),
            YfinanceClientDummy(),
        )

    def test__process(self, stocks_updater):
        stocks_updater.process()
