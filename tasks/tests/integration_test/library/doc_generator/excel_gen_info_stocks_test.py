import datetime
import importlib.resources

import pandas
from mockito import when

from library.doc_generator.excel_gen_info_stocks import ExcelCreatorForStockDetails


class TestExcelCreator:

    def test__create(self, list_stock_signals):
        c = ExcelCreatorForStockDetails(list_stock_signals)

        with when(c)._get_date().thenReturn(datetime.date(2023, 2, 15)):
            path = c.create()
            print(path)

        with importlib.resources.path("resources", "StockDetails_2023-02-15.xlsx") as path_ref:
            doc: pandas.DataFrame = pandas.read_excel(path, sheet_name=0)
            doc_ref: pandas.DataFrame = pandas.read_excel(path_ref, sheet_name=0)
            assert doc.equals(doc_ref), path
