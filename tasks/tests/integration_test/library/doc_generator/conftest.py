import pytest
from dateutil.parser import parse
from typing import List

from bondanalyzer_schemes.database_schemes import StockRead, Signal
from bondanalyzer_schemes.mixin import StockSignal


@pytest.fixture
def list_stock_signals() -> List[StockSignal]:
    return [
        StockSignal(
            stock=StockRead(
                id=1,
                name="Roper Technologies, Inc.",
                original=False,
                score=78,
                sector="Industrials",
                ticker="ROP.F",
                isin="US5717481023",
                currency="EUR",
                market_cap=2425691452793,
                geopac=0.078,
                gain_consistency=1.,
                loss_ratio=1,
                latest_price=151.92,
                latest_ma_200=143.974,
                months=119,
                ath=183.21,
            ),
            signal=Signal(
                id=1,
                stock_id=1,
                date=parse("Fri, 10 Feb 2023 00:00:00 GMT").date(),
                signal_type=1,
            )
        ),
        StockSignal(
            stock=StockRead(
                id=2,
                name="Adidas",
                original=True,
                score=12,
                sector="Consumer",
                ticker="ADI.F",
                isin="DE000A1EWWW0",
                currency="EUR",
                market_cap=163764106014,
                geopac=0.012,
                gain_consistency=1.,
                loss_ratio=1,
                latest_price=92.91,
                latest_ma_200=101.86,
                months=119,
                ath=190.12
            ),
            signal=Signal(
                id=1,
                stock_id=2,
                date=parse("Fri, 9 Feb 2023 00:00:00 GMT").date(),
                signal_type=4,
            )
        ),
    ]
