import datetime

import pandas
import yfinance


def test__yfinance_interface(df_apc_nvd_1y_raw):
    df = yfinance.download(
        ["APC.F", "NVD.F"],
        actions=False,
        group_by="ticker",
        start=datetime.date(2022, 1, 1),
        end=datetime.date(2023, 1, 1),
        period=None
    )

    assert pandas.testing.assert_frame_equal(
        df[["APC.F", "NVD.F"]],
        df_apc_nvd_1y_raw
    ) is None
