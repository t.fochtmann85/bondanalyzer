import importlib.resources

import pandas
import pytest


def _get_yfinance_dataframe_from_stored_csv(file_name: str, header) -> pandas.DataFrame:
    with importlib.resources.path("resources", file_name) as path:
        df = pandas.read_csv(path, index_col=0, header=header)
        df.index = pandas.to_datetime(df.index)
    return df


@pytest.fixture
def df_apc_nvd_1y_raw() -> pandas.DataFrame:
    return _get_yfinance_dataframe_from_stored_csv("apc_nvd_raw.csv", [0, 1])


@pytest.fixture
def df_apc_nvd_1y_reduced() -> pandas.DataFrame:
    return _get_yfinance_dataframe_from_stored_csv("apc_nvd.csv", 0)


@pytest.fixture
def df_apc_nvd_10y_reduced() -> pandas.DataFrame:
    return _get_yfinance_dataframe_from_stored_csv("apc_nvd__2012-01-01.csv", 0)


@pytest.fixture
def df_all() -> pandas.DataFrame:
    return _get_yfinance_dataframe_from_stored_csv("all.csv", 0)
