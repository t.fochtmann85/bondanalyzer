from datetime import date

import pytest

import date_helper


@pytest.mark.parametrize("d, expect", [
    pytest.param(date(2023, 4, 1), False, id="Saturday"),
    pytest.param(date(2023, 4, 2), False, id="Sunday"),
    pytest.param(date(2023, 4, 3), True, id="Monday"),
    pytest.param(date(2023, 4, 4), True, id="Tuesday"),
    pytest.param(date(2023, 4, 5), True, id="Wednesday"),
    pytest.param(date(2023, 4, 6), True, id="Thursday"),
    pytest.param(date(2023, 4, 7), False, id="Easter Friday"),
    pytest.param(date(2023, 4, 8), False, id="Saturday"),
    pytest.param(date(2023, 4, 9), False, id="Sunday"),
    pytest.param(date(2023, 4, 10), False, id="Easter Monday"),
    pytest.param(date(2023, 4, 11), True, id="Tuesday"),
])
def test__is_workday(d: date, expect):
    assert date_helper.is_workday(d) is expect


@pytest.mark.parametrize("d, expect", [
    pytest.param(date(2023, 4, 1), False, id="Satuday"),
    pytest.param(date(2023, 4, 2), False, id="Sunday"),
    pytest.param(date(2023, 4, 3), True, id="Monday"),
    pytest.param(date(2023, 4, 4), False, id="Tuesday"),
    pytest.param(date(2023, 5, 1), False),
    pytest.param(date(2023, 5, 2), True),
    pytest.param(date(2023, 5, 3), False),
    pytest.param(date(2023, 6, 1), True),
    pytest.param(date(2023, 6, 2), False),
    pytest.param(date(2023, 7, 1), False),
    pytest.param(date(2023, 7, 2), False),
    pytest.param(date(2023, 7, 3), True),
    pytest.param(date(2023, 7, 4), False),
])
def test__is_first_of_month(d: date, expect):
    assert date_helper.is_first_workday_of_month(d) is expect
