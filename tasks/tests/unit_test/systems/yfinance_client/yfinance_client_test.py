import pytest

from systems.yfinance_client.yfinance_client import YfinanceClient


@pytest.mark.parametrize("isin, matches", [
    pytest.param("US88160R1014", True, id="upper case"),
    pytest.param("us88160r1014", True, id="lower case"),
    pytest.param("u188160R1014", False, id="doesnt start with 2 alpha chars"),
    pytest.param("us88160R101a", False, id="doesnt end with numeric"),
    pytest.param("US88160R101", False, id="too short"),
    pytest.param("US88160R10111", False, id="too long"),
])
def test__matches_isin_pattern(isin, matches):
    assert YfinanceClient.matches_isin_pattern(isin) is matches


@pytest.mark.parametrize("isin, expect", [
    pytest.param("US88160R1014", "US88160R1014", id="nothing to do"),
    pytest.param(" US88160R1014 ", "US88160R1014", id="without spaces"),
    pytest.param(" - ", "", id="nothing useful"),
])
def test__get_cleaned_isin(isin, expect):
    assert YfinanceClient._get_cleaned_isin(isin) == expect
