import pytest
from mockito import when, verify, mock

from systems.alpha_vantage.alpha_vantage_client_dummy import AlphaVantageClientDummy
from systems.alpha_vantage.oracle import AlphaVantageSymbolOracle


@pytest.fixture
def client() -> AlphaVantageClientDummy:
    return AlphaVantageClientDummy()


@pytest.fixture
def av_oracle(client) -> AlphaVantageSymbolOracle:
    yield AlphaVantageSymbolOracle(client)
    AlphaVantageSymbolOracle._instances = dict()


@pytest.fixture
def response_by_ticker() -> list[dict]:
    return [{'1. symbol': 'APC.FRK', '2. name': 'Apple Inc', '3. type': 'Equity', '4. region': 'Frankfurt',
             '5. marketOpen': '08:00', '6. marketClose': '20:00', '7. timezone': 'UTC+02', '8. currency': 'EUR',
             '9. matchScore': '1.0000'}]


@pytest.fixture
def response_by_name() -> list[dict]:
    return [{'1. symbol': 'ADS1.FRK',
             '2. name': 'adidas AG',
             '3. type': 'Equity',
             '4. region': 'Frankfurt',
             '5. marketOpen': '08:00',
             '6. marketClose': '20:00',
             '7. timezone': 'UTC+02',
             '8. currency': 'EUR',
             '9. matchScore': '0.8000'},
            {'1. symbol': 'ADS.DEX',
             '2. name': 'adidas AG',
             '3. type': 'Equity',
             '4. region': 'XETRA',
             '5. marketOpen': '08:00',
             '6. marketClose': '20:00',
             '7. timezone': 'UTC+02',
             '8. currency': 'EUR',
             '9. matchScore': '0.8000'}]


def test__get_symbol_from_stock__not_known_found_by_ticker(av_oracle, client, response_by_ticker):
    stock_apc = mock({
        "name": "Apple",
        "ticker": "APC.F",
    })
    with (
        when(client).get_symbol_by_stock_ticker(...).thenReturn(response_by_ticker),
        when(av_oracle)._save(...),
    ):
        assert av_oracle.get_symbol_from_stock(stock_apc) == "APC.FRK"
        assert av_oracle.get_symbol_from_stock(stock_apc) == "APC.FRK"

        verify(client, times=1).get_symbol_by_stock_ticker("APC.FRK")
        verify(av_oracle, times=1)._save(...)


def test__get_symbol_from_stock__not_known_found_by_name(av_oracle, client, response_by_ticker):
    stock_apc = mock({
        "name": "Apple",
        "ticker": "APC.F",
    })
    with (
        when(client).get_symbol_by_stock_ticker(...).thenReturn([]),
        when(client).get_symbol_by_stock_name(...).thenReturn(response_by_ticker),
        when(av_oracle)._save(...),
    ):
        assert av_oracle.get_symbol_from_stock(stock_apc) == "APC.FRK"
        assert av_oracle.get_symbol_from_stock(stock_apc) == "APC.FRK"

        verify(client, times=1).get_symbol_by_stock_ticker("APC.FRK")
        verify(client, times=1).get_symbol_by_stock_name("Apple")
        verify(av_oracle, times=1)._save(...)


def test__get_symbol_from_stock__unknown_not_found(av_oracle, client, response_by_ticker):
    stock_apc = mock({
        "name": "Apple",
        "ticker": "APC.F",
    })
    with (
        when(client).get_symbol_by_stock_ticker(...).thenReturn([]),
        when(client).get_symbol_by_stock_name(...).thenReturn([]),
        when(av_oracle)._save(...),
    ):
        assert av_oracle.get_symbol_from_stock(stock_apc) is None

        verify(client, times=1).get_symbol_by_stock_ticker("APC.FRK")
        verify(client, times=1).get_symbol_by_stock_name("Apple")
        verify(av_oracle, times=0)._save(...)


def test__get_symbol_from_stock__known(av_oracle, client, response_by_ticker):
    stock = mock({
        "name": "Adidas",
        "ticker": "ADS.F",
    })
    with (
        when(client).get_symbol_by_stock_ticker(...).thenReturn(response_by_ticker),
        when(av_oracle)._save(...),
    ):
        assert av_oracle.get_symbol_from_stock(stock) == "ADS1.FRK"
        assert av_oracle.get_symbol_from_stock(stock) == "ADS1.FRK"

        verify(client, times=0).get_symbol_by_stock_ticker("APC.FRK")
        verify(av_oracle, times=0)._save(...)
