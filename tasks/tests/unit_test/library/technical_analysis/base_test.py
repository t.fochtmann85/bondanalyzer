import datetime
from datetime import date

import numpy
import pandas
import pytest
import typeguard

from library.technical_analysis import metrics


def test_raises__type_error(df_apc_nvd_10y_reduced: pandas.DataFrame):
    with pytest.raises(typeguard.TypeCheckError):
        metrics.MetricCalculator(df_apc_nvd_10y_reduced['APC.F'].to_numpy(), True)


@pytest.mark.parametrize("p, d, msg", [
    pytest.param([0], [0], r"Prices array only has length 1\.", id="not enough data"),
    pytest.param([0, 1, 2], ["a", "b"], r"Prices and dates are not of equal length\.", id="not same size"),
    pytest.param([0, 1], [date(2023, 1, 1), date(2022, 12, 31)], r"Dates don't seem to ascend\.",
                 id="dates not ascending"),
])
def test_raises__init(p, d, msg: str):
    with pytest.raises(AssertionError, match=msg):
        metrics.MetricCalculator(
            numpy.array(p),
            numpy.array(d),
        )


def test__init__remove_nan():
    dt = date(2017, 5, 9)
    mc = metrics.MetricCalculator(
        numpy.array([1, 2, numpy.nan, 4]),
        numpy.array([dt, dt + datetime.timedelta(1), dt + datetime.timedelta(2), dt + datetime.timedelta(3)]),
    )

    assert numpy.array_equal(mc.daily_prices, numpy.array([1, 2, 4]))
    assert numpy.array_equal(mc.daily_dates, numpy.array([dt, dt + datetime.timedelta(1), dt + datetime.timedelta(3)]))
