import datetime
import math
import pandas
import pytest

from library.technical_analysis import metrics


@pytest.fixture
def metric_calculator(df_apc_nvd_10y_reduced: pandas.DataFrame) -> metrics.MetricCalculator:
    return metrics.MetricCalculator(
        df_apc_nvd_10y_reduced['APC.F'].to_numpy(),
        df_apc_nvd_10y_reduced.index.date
    )


@pytest.mark.parametrize("days", [
    pytest.param(-1, id="negative"),
    pytest.param(0, id="zero doesn't make sense"),
    pytest.param(1, id="too few"),
])
def test_raises__get_latest_moving_average(days: int, metric_calculator):
    with pytest.raises(AssertionError, match=r"Moving average should be at least 2\."):
        metric_calculator.get_latest_moving_average(days)


def test__get_month_count(metric_calculator):
    assert metric_calculator.get_month_count() == 131


def test__get_latest_price(metric_calculator):
    assert math.isclose(metric_calculator.get_latest_price(), 120.44000244140625, rel_tol=1e-13)


def test__get_latest_moving_average(metric_calculator):
    assert math.isclose(metric_calculator.get_latest_moving_average(200), 146.3980997467041, rel_tol=1e-13)


def test__get_loss_ratio(metric_calculator):
    assert math.isclose(metric_calculator.get_loss_ratio(), 2.3732528947242657, rel_tol=1e-5)


def test__get_gain_consistency(metric_calculator):
    assert math.isclose(metric_calculator.get_gain_consistency(), 0.9429971988795518, rel_tol=1e-5)


def test__get_geopac(metric_calculator):
    assert math.isclose(metric_calculator.get_geopac(), 0.27305756889319066, rel_tol=1e-3)


def test__get_all_time_high(metric_calculator):
    assert math.isclose(metric_calculator.get_all_time_high(), 173.160, rel_tol=1e-3)


def test__mass_and_performance_test(df_all):
    start_time = datetime.datetime.now()
    for ticker in df_all.columns:
        print(f"Calculate for ticker {ticker}")
        mc = metrics.MetricCalculator(
            df_all[ticker].to_numpy(),
            df_all.index.date
        )
        assert mc.get_latest_moving_average(200) > .0
        assert mc.get_month_count() > 1
        assert mc.get_latest_price() > .0
        assert mc.get_loss_ratio() > .0
        assert .0 <= mc.get_gain_consistency() <= 1.
        assert isinstance(mc.get_geopac(), float)

    duration = (datetime.datetime.now() - start_time).total_seconds()

    print(f"Took {duration} seconds to calculate metrics for 224 stocks in the last 11 years.")
    assert duration < 1., "Should be less than one second."
