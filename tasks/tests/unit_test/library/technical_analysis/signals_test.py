import datetime
import importlib.resources
import json
from datetime import date

import numpy
import pandas
import pytest

from bondanalyzer_schemes.database_schemes import SignalType
from library.technical_analysis import signals


@pytest.fixture
def signal_calculator(df_apc_nvd_10y_reduced: pandas.DataFrame) -> signals.SignalCalculator:
    return signals.SignalCalculator(
        df_apc_nvd_10y_reduced['APC.F'].to_numpy(),
        df_apc_nvd_10y_reduced.index.date
    )


@pytest.fixture
def ma_apc_prices() -> numpy.ndarray:
    with importlib.resources.path("resources", "apc_200_prices.txt") as path:
        return numpy.loadtxt(path)


@pytest.fixture
def ma_apc_dates() -> numpy.ndarray:
    with importlib.resources.path("resources", "apc_200_dates.txt") as path:
        return numpy.loadtxt(path, encoding='latin1',
                             converters={0: lambda x: datetime.datetime.strptime(x, '%Y-%m-%d').date()},
                             dtype=date)


@pytest.fixture
def apc_buy_signal_dates() -> numpy.ndarray:
    with importlib.resources.path("resources", "apc_buy_signal_dates.txt") as path:
        return numpy.loadtxt(path, encoding='latin1',
                             converters={0: lambda x: datetime.datetime.strptime(x, '%Y-%m-%d').date()},
                             dtype=date)


@pytest.fixture
def apc_trendreversal_signal_dates() -> numpy.ndarray:
    with importlib.resources.path("resources", "apc_trendreversal_signal_dates.txt") as path:
        return numpy.loadtxt(path, encoding='latin1',
                             converters={0: lambda x: datetime.datetime.strptime(x, '%Y-%m-%d').date()},
                             dtype=date)


@pytest.fixture
def apc_buyagain_signal_dates() -> numpy.ndarray:
    with importlib.resources.path("resources", "apc_buyagain_signal_dates.txt") as path:
        return numpy.loadtxt(path, encoding='latin1',
                             converters={0: lambda x: datetime.datetime.strptime(x, '%Y-%m-%d').date()},
                             dtype=date)


@pytest.fixture
def apc_hold_signal_dates() -> numpy.ndarray:
    with importlib.resources.path("resources", "apc_hold_signal_dates.txt") as path:
        return numpy.loadtxt(path, encoding='latin1',
                             converters={0: lambda x: datetime.datetime.strptime(x, '%Y-%m-%d').date()},
                             dtype=date)


@pytest.fixture
def apc_signals() -> list[tuple[date, SignalType]]:
    with importlib.resources.path("resources", "apc_signals.json") as path:
        with path.open() as f:
            return [(datetime.datetime.strptime(a, "%Y-%m-%d").date(), SignalType(b)) for a, b in json.load(f)]


@pytest.fixture
def ma_apc_moving_average() -> numpy.ndarray:
    with importlib.resources.path("resources", "apc_200_moving_average_prices.txt") as path:
        return numpy.loadtxt(path)


def test__get_moving_average_data(ma_apc_prices, ma_apc_dates, ma_apc_moving_average, signal_calculator):
    a, b, c = signal_calculator._get_moving_average_data(200)

    assert numpy.allclose(a, ma_apc_prices)
    assert numpy.allclose(c, ma_apc_moving_average)
    assert numpy.array_equal(b, ma_apc_dates)


def test__discover_buy_signals(apc_buy_signal_dates, signal_calculator):
    dates = signal_calculator._discover_buy_signal_dates()
    # numpy.savetxt('apc_buy_signal_dates.txt', dates, fmt='%s')
    assert numpy.array_equal(dates, apc_buy_signal_dates)


def test__discover_trendreversal_signal_dates(apc_trendreversal_signal_dates, signal_calculator):
    dates = signal_calculator._discover_trendreversal_signal_dates()
    assert numpy.array_equal(dates, apc_trendreversal_signal_dates)


def test__discover_buyagain_signal_dates(apc_buyagain_signal_dates, signal_calculator):
    dates = signal_calculator._discover_buyagain_signal_dates()
    assert numpy.array_equal(dates, apc_buyagain_signal_dates)


def test__discover_hold_signal_dates(apc_hold_signal_dates, signal_calculator):
    dates = signal_calculator._discover_hold_signal_dates()
    assert numpy.array_equal(dates, apc_hold_signal_dates)


def test__signals(apc_signals, signal_calculator):
    assert signal_calculator.signals == apc_signals


def test__get_most_recent_signal(signal_calculator):
    assert signal_calculator.get_most_recent_signal() == (date(2022, 11, 2), SignalType.TRENDREVERSAL)


def test__get_most_recent_signal__no_signals(signal_calculator):
    signal_calculator._signals = list()
    assert signal_calculator.get_most_recent_signal() is None


def test__mass_and_performance_test(df_all):
    start_time = datetime.datetime.now()
    for ticker in df_all.columns:
        print(f"Calculate for ticker {ticker}")
        sc = signals.SignalCalculator(
            df_all[ticker].to_numpy(),
            df_all.index.date
        )
        assert len(sc.signals) > 0

    duration = (datetime.datetime.now() - start_time).total_seconds()

    print(f"Took {duration} seconds to calculate signals for 224 stocks in the last 11 years.")
    assert duration < 1., "Should be less than one second."
