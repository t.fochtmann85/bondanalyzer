import pytest

import consolidate
from systems.bondanalyzer_api_client.bondanalyzer_api_client_wrapper_dummy import BondanalyzerApiClientWrapperDummy
from systems.twelve_data_client.models import StockType, Stock
from systems.twelve_data_client.twelve_data_client_dummy import TwelvedataClientDummy
from systems.twelve_data_client.twelve_data_client_wrapper import TwelvedataClientWrapper


@pytest.fixture
def stocks() -> list[Stock]:
    stocks = list()
    wrapper = TwelvedataClientWrapper(TwelvedataClientDummy())
    for exchange in wrapper.get_exchanges():
        if exchange.country == "Germany":
            for stock in wrapper.get_stocks_of_exchange(exchange.code):
                if stock.type == StockType.COMMON_STOCK:
                    stocks.append(stock)
    return stocks


@pytest.fixture
def bond_acw() -> BondanalyzerApiClientWrapperDummy:
    return BondanalyzerApiClientWrapperDummy()


@pytest.mark.parametrize("name, expect", [
    pytest.param("Shandong Gold Mining Co., Ltd.", "shandonggoldminingcoltd"),
    pytest.param("Casino, Guichard-Perrachon Société Anonyme", "casinoguichardperrachonsociétéanonyme"),
])
def test__unify_name(name, expect):
    assert consolidate.StockConsolidator.unify_name(name) == expect


def test__consolidate_twelve_data_stocks__real_data(stocks):
    assert len(stocks) == 28989

    consolidated_stocks = consolidate.StockConsolidator.consolidate_twelve_data_stocks(stocks)
    assert len(consolidated_stocks) == 13852


@pytest.fixture
def td_stock_siblings() -> list[Stock]:
    return [
        Stock(**{
            "country": "Germany",
            "currency": "EUR",
            "exchange": "Munich",
            "name": "A2A S.p.A.",
            "symbol": "EAM",
            "type": "Common"
        }),
        Stock(**{
            "country": "Germany",
            "currency": "EUR",
            "exchange": "FSX",
            "name": "A2A S.p.A.",
            "symbol": "EAM",
            "type": "Common"
        }),
        Stock(**{
            "country": "Germany",
            "currency": "EUR",
            "exchange": "XBER",
            "name": "A2A S.p.A.",
            "symbol": "EAM",
            "type": "Common"
        }),
    ]


def test__consolidate_twelve_data_stocks(td_stock_siblings):
    assert consolidate.StockConsolidator.consolidate_twelve_data_stocks(td_stock_siblings) == [
        Stock(**{
            "country": "Germany",
            "currency": "EUR",
            "exchange": "FSX",
            "name": "A2A S.p.A.",
            "symbol": "EAM",
            "type": "Common"
        })
    ]


def test__reduce_twelve_data_stocks_by_bondanalyzer_stocks(stocks, bond_acw):
    consolidated_stocks = consolidate.StockConsolidator.consolidate_twelve_data_stocks(stocks)
    assert len(consolidated_stocks) == 13852

    known_stocks = bond_acw.get_stocks()
    reduced_stocks = consolidate.StockConsolidator.reduce_twelve_data_stocks_by_bondanalyzer_stocks(
        consolidated_stocks,
        known_stocks
    )

    assert len(reduced_stocks) == 13666


def __get_updated_exchanges_info():
    from systems.twelve_data_client.twelve_data_client import TwelvedataClient
    import importlib.resources
    import json
    from dotenv import load_dotenv
    load_dotenv()

    client = TwelvedataClient()

    for exchange in client.get_exchanges():
        print(exchange["country"],exchange["name"], )
        if exchange["country"] != "Germany":
            print("\tSTOP")
            continue
        exchange_code = exchange["code"]
        stocks = client.get_stocks_of_exchange(exchange_code)
        with importlib.resources.path("resources", f"stock_exchange_{exchange_code}.json") as path:
            with path.open("w") as f:
                json.dump({"data": stocks}, f, indent="  ")
