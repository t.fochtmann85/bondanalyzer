import datetime

import pandas
import pytest
from mockito import when, verify

import common
from systems.yfinance_client.yfinance_client_dummy import YfinanceClientDummy


def test__get_recipients():
    assert common.get_recipients() == [
        "t.fochtmann85@gmail.com",
        "rico.mueller1985@googlemail.com",
        "lutz-hoffmann@gmx.net",
        "lutz.hoffmann6@gmail.com"
    ]


@pytest.fixture
def yc() -> YfinanceClientDummy:
    return YfinanceClientDummy()


def test__get_prices(yc, df_apc_nvd_1y_raw, df_apc_nvd_1y_reduced):
    from_date = datetime.date(2022, 1, 1)
    end_date = datetime.date(2023, 1, 1)
    with (
        when(yc).get_ticker_prices(...).thenReturn(df_apc_nvd_1y_raw),
        when(common)._get_end_date().thenReturn(end_date)
    ):
        pandas.testing.assert_frame_equal(common.get_prices(yc, from_date, ["APC.F", "NVD.F"]), df_apc_nvd_1y_reduced)

        verify(yc).get_ticker_prices(
            ["APC.F", "NVD.F"],
            actions=False,
            group_by="ticker",
            start=from_date,
            end=end_date,
            period=None
        )
        verify(common)._get_end_date()


@pytest.mark.parametrize("d, expect", [
    pytest.param(datetime.date(2023, 1, 1), True, id="new year"),
    pytest.param(datetime.date(2023, 1, 3), False, id="not new year"),
])
def test__is_holiday(d, expect):
    assert common.is_holiday(d) is expect
