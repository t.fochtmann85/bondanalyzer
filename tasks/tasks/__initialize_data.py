import importlib.resources

import yaml
from dotenv import load_dotenv

from bondanalyzer_schemes.database_schemes import StockCreate
from system_config import ClientProvider

load_dotenv()
cp = ClientProvider()
bc = cp.get_bondanalyzer_api_client_wrapper()

with importlib.resources.path('resources', 'initial_stocks.json') as path:
    with path.open() as f:
        stocks = yaml.safe_load(f)

for stock_dict in stocks:
    try:
        s = bc.create_stock(StockCreate(**stock_dict))
        print("successful", s)
    except Exception as e:
        print(e)
