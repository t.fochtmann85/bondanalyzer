import datetime
import importlib.resources
import logging
from datetime import date

import pandas
import yaml
from pandas import DataFrame

from bondanalyzer_schemes.database_schemes import StockRead
from systems.alpha_vantage.alpha_vantage_client_interface import AlphaVantageClientInterface
from systems.alpha_vantage.oracle import AlphaVantageSymbolOracle
from systems.yfinance_client.yfinance_client_interface import YfinanceClientInterface

logger = logging.getLogger(__name__)


def get_recipients() -> list[str]:
    with importlib.resources.path("resources", "recipients.yaml") as path:
        with path.open() as f:
            return yaml.safe_load(f)


def get_prices(yc: YfinanceClientInterface, from_date: date, tickers: list[str]) -> pandas.DataFrame:
    df: DataFrame = yc.get_ticker_prices(
        tickers,
        actions=False,
        group_by="ticker",
        start=from_date,
        end=_get_end_date(),
        period=None
    )

    if not isinstance(df.index, pandas.DatetimeIndex):
        df.index = pandas.to_datetime(df.index)

    df_new = pandas.DataFrame()
    df_new.index = df.index

    for multi_col, series in df.items():  # type: tuple[str ,str], pandas.Series
        if multi_col[1] == 'Close':
            df_new[multi_col[0]] = series

    logger.debug(f"Received {df_new.size} price points.")

    return df_new


def _get_end_date() -> datetime.date:
    return datetime.date.today() + datetime.timedelta(days=1)


def get_market_cap(stock: StockRead, yc: YfinanceClientInterface, avc: AlphaVantageClientInterface):
    yfinance_ticker = yc.get_ticker_info(stock.ticker)

    try:
        market_cap = yfinance_ticker.get('marketCap', 0)
    except Exception as exc:
        logger.error(f"When trying to discover market capitalization of {stock.ticker} "
                     f"using yfinance and error ocurred:\n{exc}")
        market_cap = 0

    if market_cap:
        logger.debug(f"Discovered market cap for {stock.ticker!r} from yfinance: {market_cap}")
        return market_cap

    av_symbol = AlphaVantageSymbolOracle(avc).get_symbol_from_stock(stock)
    if not av_symbol:
        return 0
    logger.debug(f"Discovered stock symbol for ticker {stock.ticker!r} from alpha vantage: {av_symbol}")

    av_data = avc.get_fundamental(av_symbol)
    market_cap = av_data.get('MarketCapitalization', 0)
    if market_cap is None:
        logger.warning(f"Unable to discover market capitalization for ticker {stock.ticker!r} from alpha vantage.")
        return 0

    logger.debug(f"Discovered market cap for {stock.ticker!r} from alpha vantage: {market_cap}")

    return int(market_cap)


def is_holiday(some_date: date) -> bool:
    with importlib.resources.path("resources", "holidays.yaml") as path:
        with path.open() as f:
            holidays = yaml.safe_load(f)

    return some_date in holidays
