import logging

from dotenv import load_dotenv

import system_config
from updater import UpdaterOfInnerStocks

logging.basicConfig(level=logging.DEBUG)

load_dotenv()

if __name__ == '__main__':
    cp = system_config.ClientProvider()
    updater = UpdaterOfInnerStocks(
        cp.get_bondanalyzer_api_client_wrapper(),
        cp.get_alpha_vantange_api_client(),
        cp.get_yfinance_client()
    )
    updater.process()
