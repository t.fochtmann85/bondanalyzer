import datetime

import common


def is_workday(date_: datetime.date) -> bool:
    return all([
        _is_weekday(date_),
        not common.is_holiday(date_)
    ])


def is_first_workday_of_month(date_: datetime.date) -> bool:
    days_evaluation = list()

    for i in range(date_.day):
        d = datetime.date(date_.year, date_.month, i + 1)
        days_evaluation.append(
            _is_weekday(d) and not common.is_holiday(d)
        )

    if True in days_evaluation:
        first_of_month_index = days_evaluation.index(True)
        return (first_of_month_index + 1) == date_.day

    return False


def _is_weekday(date_: datetime.date) -> bool:
    return 1 <= date_.isoweekday() <= 5
