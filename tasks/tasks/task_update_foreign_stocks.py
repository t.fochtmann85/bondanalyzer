import datetime

from dotenv import load_dotenv

import system_config
from log import setup_logging
from updater import UpdaterOfForeignStocks


def _get_today() -> datetime.date:
    return datetime.date.today()


if __name__ == '__main__':
    # prepare
    load_dotenv()
    setup_logging()
    cp = system_config.ClientProvider()

    # update
    print("Update stocks...")
    updater = UpdaterOfForeignStocks(
        cp.get_twelve_data_api_client_wrapper(),
        cp.get_bondanalyzer_api_client_wrapper(),
        cp.get_alpha_vantange_api_client(),
        cp.get_yfinance_client()
    )
    updater.process()

    print("Script finished.")
