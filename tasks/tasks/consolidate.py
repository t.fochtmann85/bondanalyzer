import logging
import re

from bondanalyzer_schemes.database_schemes import StockRead
from systems.twelve_data_client.models import Stock

logger = logging.getLogger(__name__)


class StockConsolidator:
    @classmethod
    def consolidate_twelve_data_stocks(cls, stocks: list[Stock]) -> list[Stock]:
        stock_dict_name = dict()
        stock_dict_symbol = dict()

        # Frankfurt is in favour
        for stock in stocks:
            if stock.exchange == "FSX":
                stock_dict_name[cls.unify_name(stock.name)] = stock
                stock_dict_symbol[stock.symbol] = stock

        # now, all others
        for stock in stocks:
            name = cls.unify_name(stock.name)
            if all([
                name not in stock_dict_name,
                stock.symbol not in stock_dict_symbol
            ]):
                stock_dict_name[name] = stock
                stock_dict_symbol[stock.symbol] = stock

        return list(stock_dict_name.values())

    @classmethod
    def unify_name(cls, name: str) -> str:
        return re.sub(r"\W", "", name).lower()

    @classmethod
    def reduce_twelve_data_stocks_by_bondanalyzer_stocks(cls, stocks: list[Stock],
                                                         other_stocks: list[StockRead]) -> list[Stock]:
        known_symbols = [elem.ticker.split(".")[0] for elem in other_stocks]
        return list(filter(lambda elem: elem.symbol not in known_symbols, stocks))

    @classmethod
    def reduce_bondanalyzer_stocks_by_twelve_data_stocks(cls, stocks: list[StockRead],
                                                         other_stocks: list[Stock]) -> list[StockRead]:
        logger.info(f"Determine stocks to delete. Having {len(stocks)} in bondanalyzer and {len(other_stocks)} "
                    "stocks that are foreign and need an update.")

        known_symbols = [elem.symbol for elem in other_stocks]
        kept_symbols = set()
        stocks_to_delete = list()

        # iterate through stocks from frankfurt first
        for ba_stock in filter(lambda s: s.ticker.endswith(".F"), stocks):
            symbol = ba_stock.ticker.split(".")[0]
            if symbol in known_symbols:
                kept_symbols.add(symbol)
                continue

            stocks_to_delete.append(ba_stock)

        # and then through all others
        for ba_stock in filter(lambda s: not s.ticker.endswith(".F"), stocks):
            symbol = ba_stock.ticker.split(".")[0]
            if symbol in known_symbols:
                if symbol not in kept_symbols:
                    kept_symbols.add(symbol)
                    continue
            stocks_to_delete.append(ba_stock)

        logger.info(f"Discovered {len(stocks_to_delete)} stocks to delete.")
        return stocks_to_delete
