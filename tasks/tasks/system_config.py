import importlib.resources

import yaml

from systems.alpha_vantage.alpha_vantage_client import AlphaVantageClient
from systems.alpha_vantage.alpha_vantage_client_dummy import AlphaVantageClientDummy
from systems.alpha_vantage.alpha_vantage_client_interface import AlphaVantageClientInterface
from systems.bondanalyzer_api_client.bondanalyzer_api_client_wrapper import BondanalyzerApiClientWrapper
from systems.bondanalyzer_api_client.bondanalyzer_api_client_wrapper_dummy import BondanalyzerApiClientWrapperDummy
from systems.bondanalyzer_api_client.bondanalyzer_api_client_wrapper_interface import \
    BondanalyzerApiClientWrapperInterface
from systems.twelve_data_client.twelve_data_client import TwelvedataClient
from systems.twelve_data_client.twelve_data_client_dummy import TwelvedataClientDummy
from systems.twelve_data_client.twelve_data_client_wrapper import TwelvedataClientWrapper
from systems.yfinance_client.yfinance_client import YfinanceClient
from systems.yfinance_client.yfinance_client_dummy import YfinanceClientDummy
from systems.yfinance_client.yfinance_client_interface import YfinanceClientInterface


class ClientProvider:
    def __init__(self):
        with importlib.resources.path('resources', 'system_config.yaml') as path:
            with path.open() as f:
                self._system_config = yaml.safe_load(f)

    def get_bondanalyzer_api_client_wrapper(self) -> BondanalyzerApiClientWrapperInterface:
        if self._system_config['dummy']['bondanalyzer_client']:
            return BondanalyzerApiClientWrapperDummy()
        return BondanalyzerApiClientWrapper()

    def get_alpha_vantange_api_client(self) -> AlphaVantageClientInterface:
        if self._system_config['dummy']['alpha_vantage_client']:
            return AlphaVantageClientDummy()
        return AlphaVantageClient()

    def get_twelve_data_api_client_wrapper(self) -> TwelvedataClientWrapper:
        if self._system_config['dummy']['twelve_data_client']:
            client = TwelvedataClientDummy()
        else:
            client = TwelvedataClient()
        return TwelvedataClientWrapper(client)

    def get_yfinance_client(self) -> YfinanceClientInterface:
        if self._system_config['dummy']['yfinance_client']:
            return YfinanceClientDummy()
        return YfinanceClient()
