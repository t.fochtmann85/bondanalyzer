import datetime

import sys
from dotenv import load_dotenv

import date_helper
import system_config
from log import setup_logging
from publisher import SignalPublisher, StockDetailPublisher
from updater import UpdaterOfInnerStocks


def _get_today() -> datetime.date:
    return datetime.date.today()


if __name__ == '__main__':
    today = _get_today()
    if not date_helper.is_workday(today):
        print("Script should not be executed")
        sys.exit()

    # prepare
    load_dotenv()
    setup_logging()
    cp = system_config.ClientProvider()
    bondanalyzer_client = cp.get_bondanalyzer_api_client_wrapper()

    # update
    print("Update stocks...")
    updater = UpdaterOfInnerStocks(
        bondanalyzer_client,
        cp.get_alpha_vantange_api_client(),
        cp.get_yfinance_client()
    )
    updater.process()

    # publish
    print("Notify users...")
    publisher_signals = SignalPublisher(
        bondanalyzer_client,
    )
    publisher_signals.publish()

    if date_helper.is_first_workday_of_month(today):
        publisher_stocks = StockDetailPublisher(
            bondanalyzer_client,
        )
        publisher_stocks.publish()

    print("Script finished.")
