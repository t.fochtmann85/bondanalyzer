import logging.config
import os
from pathlib import Path


def setup_logging():
    log_level = os.environ['LOG_LEVEL']
    print(f"Set logging level to {log_level}")
    logging.config.fileConfig(Path(__file__).parent / "logging.ini")
