import datetime
import logging
from abc import abstractmethod
from pathlib import Path
from typing import Any

import common
from library.doc_generator import excel_gen_latest_signals, excel_gen_info_stocks
from mail_sender.sender import Sender
from systems.bondanalyzer_api_client.bondanalyzer_api_client_wrapper_interface import \
    BondanalyzerApiClientWrapperInterface

logger = logging.getLogger(__name__)


class Publisher:
    def __init__(self, bc: BondanalyzerApiClientWrapperInterface):
        self._bc = bc

    @property
    @abstractmethod
    def subject(self) -> str:
        pass

    @property
    @abstractmethod
    def body(self) -> str:
        pass

    @staticmethod
    def get_recipients() -> list[str]:
        return common.get_recipients()

    def publish(self):
        source_data = self._get_source_data()
        path_file = self._get_file(source_data)
        self._notify(path_file)

    def _get_source_data(self) -> Any:
        stock_signals = self._bc.get_signals()
        logger.info(f"Received {len(stock_signals)} stock signals to publish.")
        if len(stock_signals) == 0:
            logger.error("No signal to announce. Check API or database.")
            return []
        return stock_signals

    @abstractmethod
    def _get_file(self, data) -> Path:
        pass

    def _notify(self, path_file: Path) -> None:
        sender = Sender(self.subject, self.body, self.get_recipients())
        sender.add_attachments(
            path_file,
            "application", "table/xlsx",
            {"Content-Disposition": f"attachment; filename= {path_file.name}"}
        )
        sender.send()


class SignalPublisher(Publisher):
    @property
    def subject(self) -> str:
        return f"[Bondanalyzer] Aktiensignale vom {datetime.date.today()}"

    @property
    def body(self) -> str:
        return "Siehe Anhang"

    def _get_file(self, data) -> Path:
        path_doc = excel_gen_latest_signals.ExcelCreatorForSignals(data).create()
        logger.info(f"Build document for stock signals at {path_doc.as_posix()}.")
        return path_doc


class StockDetailPublisher(Publisher):
    @property
    def subject(self) -> str:
        return f"[Bondanalyzer] Aktiendetails Monat {datetime.date.today().strftime('%B')}"

    @property
    def body(self) -> str:
        return "Siehe Anhang"

    def _get_file(self, data) -> Path:
        path_doc = excel_gen_info_stocks.ExcelCreatorForStockDetails(data).create()
        logger.info(f"Build document for stock details at {path_doc.as_posix()}.")
        return path_doc
