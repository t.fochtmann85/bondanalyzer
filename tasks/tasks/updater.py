import asyncio
import datetime
import logging
from abc import abstractmethod
from more_itertools import batched
from pandas import Series
from uuid import uuid4

import common
import consolidate
from bondanalyzer_schemes.database_schemes import StockRead, StockUpdate, StockCreate, SignalCreate, StockFilter
from library.technical_analysis import metrics, signals
from systems.alpha_vantage.alpha_vantage_client_interface import AlphaVantageClientInterface
from systems.bondanalyzer_api_client.bondanalyzer_api_client_wrapper_interface import \
    BondanalyzerApiClientWrapperInterface
from systems.twelve_data_client.models import Stock, StockType
from systems.twelve_data_client.twelve_data_client_wrapper import TwelvedataClientWrapper
from systems.yfinance_client.yfinance_client_interface import YfinanceClientInterface

logger = logging.getLogger(__name__)


class UpdaterBase:
    def __init__(self, bc: BondanalyzerApiClientWrapperInterface, ac: AlphaVantageClientInterface,
                 yc: YfinanceClientInterface):
        self._bc = bc
        self._ac = ac
        self._yc = yc

        self._stocks = None
        # self._queue = Queue()
        # self._worker = Thread(target=update_market_cap, args=(self._queue, self._bc, self._yc, self._ac))

    @property
    def stocks(self) -> dict[str, StockRead]:
        if self._stocks is None:
            self._stocks = self._get_stocks()
        return self._stocks

    def process(self) -> None:
        if len(self.stocks) == 0:
            logger.warning("No stocks available.")
            return
        logger.info(f"Update metrics for {len(self.stocks)} stocks.")

        for ticker_batch in batched(list(self.stocks.keys()), 50):
            self._process_batch(ticker_batch)
        # self._queue.put(None)
        # self._queue.join()

    def _process_batch(self, tickers: list[str]):
        price_data = common.get_prices(
            self._yc,
            datetime.date.today() - datetime.timedelta(days=365 * 10),
            tickers
        )
        if price_data is None:
            logger.warning("No price data was collected.")
            return

        for ticker in tickers:
            if ticker in price_data.columns:
                self._process_stock(self.stocks[ticker], price_data[ticker].dropna())
            else:
                logger.error(f"Ticker {ticker!r} is not available in price data.")

        logger.info("Stocks updated")

    @abstractmethod
    def _process_stock(self, stock: StockRead, price_data: Series) -> None:
        pass

    def _update_stock_metrics(self, stock: StockRead, price_data: Series) -> None:
        if price_data.size <= 60:
            logger.warning(f"Too few price points for stock {stock.name!r}. Can't calculate metrics.")
            return None

        mc = metrics.MetricCalculator(price_data.array.to_numpy(), price_data.index.date)

        stock_update = StockUpdate(
            geopac=mc.get_geopac(),
            gain_consistency=mc.get_gain_consistency(),
            loss_ratio=mc.get_loss_ratio(),
            latest_price=mc.get_latest_price(),
            latest_ma_200=mc.get_latest_moving_average(days=200),
            months=mc.get_month_count(),
            ath=mc.get_all_time_high()
        )

        stock_new = self._bc.update_stock(stock.ticker, stock_update)
        logger.info(f"Update stock metrics of {stock_new.name!r}")
        # self._queue.put(stock)
        # if not self._worker.is_alive():
        #     self._worker.start()

    @abstractmethod
    def _get_stocks(self) -> dict[str, StockRead]:
        pass


class UpdaterOfInnerStocks(UpdaterBase):
    def _process_stock(self, stock: StockRead, price_data: Series) -> None:
        print(f"Process stock {stock.ticker}.")
        self._update_stock_metrics(stock, price_data)
        self._update_stock_signals(stock, price_data)

    def _get_stocks(self) -> dict[str, StockRead]:
        return {s.ticker: s for s in self._bc.get_stocks(inner=True)}

    def _update_stock_signals(self, stock: StockRead, price_data: Series) -> None:
        if price_data.size <= 200:
            logger.warning(f"Too few price points for stock {stock.name!r}. Can't calculate signals.")
            return None

        sc = signals.SignalCalculator(price_data.array.to_numpy(), price_data.index.date)

        last_signal_info = sc.get_most_recent_signal()
        if last_signal_info is not None:
            signal_create = SignalCreate(
                date=last_signal_info[0],
                signal_type=last_signal_info[1]
            )
            signal_new = self._bc.create_stock_signal(stock.ticker, signal_create)
            if signal_new is not None:
                logger.info(f"Added new signal {signal_create.signal_type.name!r} to stock {stock.name!r}.")
            else:
                logger.debug(f"Signal {signal_create.signal_type.name!r} on {signal_create.date} of "
                             f"stock {stock.name!r} already known.")
        else:
            logger.warning(f"No signals discovered for stock {stock.name!r}.")


class UpdaterOfForeignStocks(UpdaterBase):
    EXCHANGE_ABBREV = {
        "XBER": "BE",
        "XETR": "DE",
        "XSTU": "SG",
        "XHAN": "HA",
        "XDUS": "DU",
        "XHAM": "HM",
        "FSX": "F",
        "Munich": "MU"
    }

    def __init__(self, tdc: TwelvedataClientWrapper, *args):
        super().__init__(*args)
        self._tdc = tdc

    def _process_stock(self, stock: StockRead, price_data: Series) -> None:
        self._update_stock_metrics(stock, price_data)

    def _get_stocks(self) -> dict[str, StockRead]:
        self._prepare_data_base()

        return {s.ticker: s for s in self._bc.get_stocks(inner=False)}

    def _prepare_data_base(self):
        german_td_stocks = list(self.__get_german_common_stocks())
        logger.info(f"Twelve data knows {len(german_td_stocks)} stocks on german exchanges.")

        german_td_stocks_consolidated = consolidate.StockConsolidator.consolidate_twelve_data_stocks(german_td_stocks)
        logger.info(f"Twelve data knows {len(german_td_stocks)} consolidated stocks on german exchanges.")

        outer_ba_stocks = self._bc.get_stocks(False)
        logger.info(f"Bondanalyzer knows {len(outer_ba_stocks)} foreign stocks.")

        self.__delete_stocks(german_td_stocks_consolidated, outer_ba_stocks)
        self.__add_stocks(german_td_stocks_consolidated, outer_ba_stocks)

    def __add_stocks(self, german_td_stocks: list[Stock], outer_ba_stocks: list[StockRead]) -> None:
        ba_stocks_to_add = consolidate.StockConsolidator.reduce_twelve_data_stocks_by_bondanalyzer_stocks(
            german_td_stocks,
            self._bc.get_stocks(True) + outer_ba_stocks
        )
        logger.info(f"Stocks to add to the data base: {len(ba_stocks_to_add)}.")

        for stock in ba_stocks_to_add:
            ticker = f"{stock.symbol}.{self.EXCHANGE_ABBREV[stock.exchange]}"
            isin = self._yc.get_isin_by_ticker(ticker)
            if isin:
                stocks_with_isin = self._bc.filter_stocks(StockFilter(isin=isin))
                if len(stocks_with_isin) > 0:
                    logger.warning(f"Stock with ISIN {isin!r} already known. Skip creation.")
                    continue
            else:
                isin = uuid4().hex[:12]

            try:
                stock_read = self._bc.create_stock(StockCreate(
                    inner=False,
                    name=stock.name,
                    ticker=ticker,
                    currency=stock.currency,
                    isin=isin
                ))
                logger.debug(f"Added new foreign stock {stock_read} to database.")
            except Exception as e:
                logger.error(f"Exception occurred while trying to create stock for ticker {ticker!r}:\n{e}\n"
                             "Will be skipped.")
        else:
            logger.debug("Finished adding stocks.")

    def __delete_stocks(self, german_td_stocks: list[Stock], outer_ba_stocks: list[StockRead]) -> None:
        ba_stocks_to_delete = consolidate.StockConsolidator.reduce_bondanalyzer_stocks_by_twelve_data_stocks(
            outer_ba_stocks,
            german_td_stocks
        )
        logger.info(f"Stocks to delete from database: {len(ba_stocks_to_delete)}.")

        for b_stock in ba_stocks_to_delete:
            self._bc.delete_stock(b_stock.ticker)
            logger.debug(f"Deleted foreign stock {b_stock.ticker} from database.")

    def __get_german_common_stocks(self) -> list[Stock]:
        for exchange in self._tdc.get_exchanges():
            if exchange.country != "Germany":
                continue

            for stock in self._tdc.get_stocks_of_exchange(exchange.code):
                if stock.type != StockType.COMMON_STOCK:
                    continue
                if stock.name.strip() == "":
                    continue
                if stock.symbol.strip() == "":
                    continue
                if self._yc.matches_isin_pattern(stock.symbol):
                    continue

                yield stock


def update_market_cap(queue: asyncio.Queue, bc: BondanalyzerApiClientWrapperInterface,
                      yc: YfinanceClientInterface, ac: AlphaVantageClientInterface):
    print('Update market cap: Running')
    # consume work
    while True:
        stock: StockRead = queue.get()
        # check for stop
        if stock is None:
            break
        # report
        print(f'>got {stock}')

        stock_update = StockUpdate(
            market_cap=common.get_market_cap(stock, yc, ac)
        )
        stock_new = bc.update_stock(stock.ticker, stock_update)
        logger.info(f"Update stock metrics of {stock_new.name!r}")
        queue.task_done()
    queue.task_done()
    # all done
    print('Consumer: Done')
