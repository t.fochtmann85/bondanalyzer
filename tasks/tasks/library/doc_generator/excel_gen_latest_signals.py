import logging
from typing import List, Iterable

from bondanalyzer_schemes.mixin import StockSignal
from library.doc_generator.excel_creator_base import ExcelCreatorBase, Color

logger = logging.getLogger(__name__)


class ExcelCreatorForSignals(ExcelCreatorBase):
    COL_DEF = [
        # (alignment, col_width, font_color, number format)
        ("left", 15, None, None),  # A
        ("left", 15, None, None),  # B
        ("right", 10, None, "0"),  # C
        ("right", 4, None, None),  # D
        ("left", 30, None, None),  # E
        ("left", 30, None, None),  # F
    ]

    def __init__(self, stock_signals: List[StockSignal]):
        super().__init__()
        self._stock_signals = stock_signals

    def _get_file_name(self) -> str:
        return f"StockSignals_{self._get_date()}.xlsx"

    def _get_sheet_name(self) -> str:
        return "Signale"

    def _get_header(self) -> list[str]:
        return ["Datum", "Signal", "Score", "", "Aktie", "Branche"]

    def _iter_data(self) -> Iterable:
        return sorted(self._stock_signals, key=lambda stock_signal: stock_signal.signal.date, reverse=True)

    def _add_row(self, row_id: int, stock_signal: StockSignal) -> None:
        self.ws.append([
            stock_signal.signal.date,
            stock_signal.signal.signal_type.name,
            stock_signal.stock.score,
            "⭐" if stock_signal.stock.original else "",
            stock_signal.stock.name,
            stock_signal.stock.sector
        ])

        self._color_row(
            {"fgColor": Color.LIGHT_GREY.value if row_id % 2 == 0 else Color.GREY.value},
            {"color": Color.BLACK.value}
        )

        # set generic stuff
        for col_id, (alignment, col_width, font_kwargs, nf) in enumerate(self.COL_DEF):
            cell = self.ws.cell(self.ws.max_row, col_id + 1)
            self._format_cell(cell, alignment, font_kwargs, nf)
