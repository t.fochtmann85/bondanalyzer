import datetime
import logging
import tempfile
from abc import abstractmethod
from enum import Enum
from pathlib import Path
from typing import Iterable

from openpyxl.cell import Cell
from openpyxl.styles import Alignment, Font, PatternFill
from openpyxl.utils import get_column_letter
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet

logger = logging.getLogger(__name__)


class Color(Enum):
    BLACK = "000000"
    WHITE = "FFFFFF"
    LIGHT_GREY = "E5E8E8"
    GREY = "D5D8DC"
    LIGHT_GREEN = "ABEBC6"
    GREEN = "28B463"
    LIGHT_ORANGE = "FAD7A0"
    LIGHT_RED = "E6B0AA"
    RED = "E53935"


class ExcelCreatorBase:
    COL_DEF = None

    def __init__(self):
        self._wb = None
        self._ws = None

    @property
    def wb(self) -> Workbook:
        if self._wb is None:
            self._wb = Workbook()
        return self._wb

    @property
    def ws(self) -> Worksheet:
        if self._ws is None:
            self._ws: Worksheet = self.wb.active
        return self._ws

    def create(self) -> Path:
        logging.debug("Start generating excel...")

        self._prepare_sheet()

        for row_id, stock_signal in enumerate(self._iter_data()):
            self._add_row(row_id, stock_signal)

        path = Path(tempfile.gettempdir()) / self._get_file_name()
        self.wb.save(path)
        return path

    @abstractmethod
    def _get_file_name(self) -> str:
        pass

    @abstractmethod
    def _get_sheet_name(self) -> str:
        pass

    @abstractmethod
    def _get_header(self) -> list[str]:
        pass

    def _prepare_sheet(self) -> None:
        self.ws.title = self._get_sheet_name()
        self.ws.append(self._get_header())
        self._color_row({"fgColor": Color.BLACK.value}, {"color": Color.WHITE.value, "bold": True})

        for col_id, (alignment, col_width, font_kwargs, nf) in enumerate(self.COL_DEF):
            cell = self.ws.cell(self.ws.max_row, col_id + 1)
            self._format_cell(cell, alignment, None, None)

        for col_id, (alignment, col_width, font_kwargs, nf) in enumerate(self.COL_DEF):
            self.ws.column_dimensions[get_column_letter(col_id + 1)].width = col_width

    @abstractmethod
    def _iter_data(self) -> Iterable:
        pass

    @abstractmethod
    def _add_row(self, row_id: int, data) -> None:
        pass

    @staticmethod
    def _format_cell(cell: Cell, alignment: str, font_kwargs: dict | None, number_format: str | None) -> None:
        cell.alignment = Alignment(horizontal=alignment)
        if font_kwargs:
            cell.font = Font(**font_kwargs, size=8, name="Verdana")
        if number_format:
            cell.number_format = number_format

    def _color_row(self, pattern_kwargs: dict | None, font_kwargs: dict | None) -> None:
        font = Font(**{"name": "Verdana", "size": 8, **font_kwargs})
        fill = PatternFill("solid", **pattern_kwargs) if pattern_kwargs else None

        for row in self.ws.iter_rows(min_row=self.ws.max_row, max_row=self.ws.max_row):
            for cell in row:  # type: Cell
                if fill:
                    cell.fill = fill
                cell.font = font

    @staticmethod
    def _get_date() -> datetime.date:
        return datetime.date.today()
