import logging
from openpyxl.styles import Font, PatternFill
from typing import Iterable

from bondanalyzer_schemes.mixin import StockSignal
from library.doc_generator.excel_creator_base import Color, ExcelCreatorBase

logger = logging.getLogger(__name__)


class ExcelCreatorForStockDetails(ExcelCreatorBase):
    COL_DEF = [
        # (alignment, col_width, font_color, number format)
        ("right", 5, None, None),  # A
        ("right", 6, None, None),  # B
        ("left", 4, {"color": "F1C40F"}, None),  # C
        ("right", 4, None, None),  # D
        ("left", 30, None, None),  # E
        ("left", 30, None, None),  # F
        ("right", 10, None, None),  # G
        ("left", 10, None, None),  # H
        ("left", 5, None, None),  # I
        ("right", 10, None, '0 %'),  # J
        ("left", 5, {"color": "6495ED"}, None),  # K
        ("right", 10, None, '0 %'),  # L
        ("left", 5, {"color": "8E44AD"}, None),  # M
        ("right", 10, None, '0.0'),  # M
        ("left", 5, {"color": "DE3163"}, None),  # O
        ("right", 15, None, '#,##0.0'),  # P
        ("right", 15, {"bold": True}, '0.00 €'),  # Q
        ("right", 15, None, '0.00 €'),  # R
        ("right", 15, None, '0.0 %'),  # S
        ("right", 15, None, '0.00 €'),  # T
        ("right", 15, None, '0.0 %'),  # U
        ("center", 10, None, None),  # V
        ("left", 20, None, None),  # W
        ("left", 15, None, None),  # X
    ]

    BEST = 25

    def __init__(self, stock_signals: list[StockSignal]):
        super().__init__()
        self._stock_signals = stock_signals

        # build positions
        stocks = [ss.stock for ss in self._stock_signals]
        self._pos_total = [elem.id for elem in sorted(stocks, key=lambda item: item.score, reverse=True)]
        self._pos_gp = [elem.id for elem in sorted(stocks, key=lambda item: item.geopac, reverse=True)]
        self._pos_gc = [elem.id for elem in sorted(stocks, key=lambda item: item.gain_consistency, reverse=True)]
        self._pos_lr = [elem.id for elem in sorted(stocks, key=lambda item: item.loss_ratio)]

    def _get_file_name(self) -> str:
        return f"StockDetails_{self._get_date()}.xlsx"

    def _get_sheet_name(self) -> str:
        return "Aktiendetails"

    def _get_header(self) -> list[str]:
        return [
            "Pos.", "Score", "", "", "Name", "Branche", "Monate", "Ticker", "Land", "geoPAK", "", "GewKon", "",
            "Verlust-Ratio", "", "Marktkap. [Mrd.]", "Akt. Kurs", "ATH", "ATH-Abweichung", "Akt. GD200", "Abweichung",
            "Trend", "Letztes Signal", ""
        ]

    def _iter_data(self) -> Iterable:
        return self._stock_signals

    def _add_row(self, row_id: int, stock_signal: StockSignal) -> None:
        stock = stock_signal.stock
        signal = stock_signal.signal

        pos_total = self._pos_total.index(stock.id) + 1
        pos_gp = self._pos_gp.index(stock.id) + 1
        pos_gc = self._pos_gc.index(stock.id) + 1
        pos_lr = self._pos_lr.index(stock.id) + 1

        self.ws.append([
            pos_total,
            stock.score,
            "⭐" if pos_total <= self.BEST else "",
            "⭐" if stock.original else "",
            stock.name,
            stock.sector,
            stock.months,
            '=HYPERLINK("https://ca.finance.yahoo.com/quote/{0}", "{0}")'.format(stock.ticker),
            stock.isin[:2].upper(),
            stock.geopac,
            "⭐" if pos_gp <= self.BEST else "",
            stock.gain_consistency,
            "⭐" if pos_gc <= self.BEST else "",
            stock.loss_ratio,
            "⭐" if pos_lr <= self.BEST else "",
            stock.market_cap / 1e+9,
            stock.latest_price,
            stock.ath,
            stock.latest_price / stock.ath - 1,
            stock.latest_ma_200,
            stock.latest_price / stock.latest_ma_200 - 1,
            "" if stock.latest_price > stock.latest_ma_200 else "",
            signal.signal_type.name,
            signal.date,
        ])

        self._color_row(
            {"fgColor": Color.LIGHT_GREY.value if row_id % 2 == 0 else Color.GREY.value},
            {"color": Color.BLACK.value}
        )

        # set generic stuff
        for col_id, (alignment, col_width, font_kwargs, nf) in enumerate(self.COL_DEF):
            cell = self.ws.cell(self.ws.max_row, col_id + 1)
            self._format_cell(cell, alignment, font_kwargs, nf)

        # set stuff based on cell values

        # I. Trend up or down
        cell_trend_ratio = self.ws.cell(self.ws.max_row, 19)
        cell_trend = self.ws.cell(self.ws.max_row, 20)
        if cell_trend_ratio.value >= 0:
            cell_trend_ratio.font = Font(color=Color.GREEN.value, size=8, name="Verdana")
            cell_trend.font = Font(color=Color.GREEN.value, size=8, name="Verdana")
        else:
            cell_trend_ratio.font = Font(color=Color.RED.value, size=8, name="Verdana")
            cell_trend.font = Font(color=Color.RED.value, size=8, name="Verdana")

        # II. Months
        cell_months = self.ws.cell(self.ws.max_row, 7)
        if cell_months.value >= 120:
            cell_months.fill = PatternFill("solid", fgColor=Color.LIGHT_GREEN.value)
        elif cell_months.value >= 100:
            cell_months.fill = PatternFill("solid", fgColor=Color.LIGHT_ORANGE.value)
        else:
            cell_months.fill = PatternFill("solid", fgColor=Color.LIGHT_RED.value)
