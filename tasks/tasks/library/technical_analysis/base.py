import numpy
import typeguard


class BaseCalculator:
    @typeguard.typechecked
    def __init__(self, daily_prices: numpy.ndarray, daily_dates: numpy.ndarray):
        assert daily_prices.size > 1, f"Prices array only has length {daily_prices.size}."
        assert daily_prices.size == daily_dates.size, "Prices and dates are not of equal length."
        assert daily_dates[0] < daily_dates[-1], "Dates don't seem to ascend."

        not_nan_indexes = numpy.where(numpy.isnan(daily_prices) == False)[0]
        self.daily_prices = daily_prices[not_nan_indexes]
        self.daily_dates = daily_dates[not_nan_indexes]
