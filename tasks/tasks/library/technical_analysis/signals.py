from datetime import date, timedelta
from functools import lru_cache

import numpy
import typeguard

from bondanalyzer_schemes.database_schemes import SignalType
from library.technical_analysis.base import BaseCalculator


class SignalCalculator(BaseCalculator):
    @typeguard.typechecked
    def __init__(self, daily_prices: numpy.ndarray, daily_dates: numpy.ndarray):
        super().__init__(daily_prices, daily_dates)

        self._signals = None

    @property
    def signals(self) -> list[tuple[date, SignalType]]:
        if self._signals is None:
            self._signals = [(d, SignalType.BUY) for d in self._discover_buy_signal_dates()]
            self._signals += [(d, SignalType.BUYAGAIN) for d in self._discover_buyagain_signal_dates()]
            self._signals += [(d, SignalType.HOLD) for d in self._discover_hold_signal_dates()]
            self._signals += [(d, SignalType.TRENDREVERSAL) for d in self._discover_trendreversal_signal_dates()]
        return sorted(self._signals, key=lambda elem: elem[0])

    @lru_cache(maxsize=4)
    def _get_moving_average_data(self, days: int) -> tuple[numpy.ndarray, numpy.ndarray, numpy.ndarray]:
        moving_average = numpy.convolve(self.daily_prices, numpy.ones(days) / days, "valid")
        return self.daily_prices[-moving_average.size:], self.daily_dates[-moving_average.size:], moving_average

    def _discover_buy_signal_dates(self) -> numpy.ndarray:
        prices, dates, ma200 = self._get_moving_average_data(200)
        above_ma200_indexes = numpy.where(prices > ma200)[0]
        return self.__get_first_of_each_window(above_ma200_indexes, dates)

    def _discover_buyagain_signal_dates(self) -> numpy.ndarray:
        month_date_sequences, month_ma200_sequences, month_price_sequences = self.__get_monthly_sequences()

        return numpy.array(
            [month_date_sequences[j + 2][0] for j in filter(
                lambda i: all([
                    month_price_sequences[i][-1] > month_price_sequences[i + 1][-1],
                    month_price_sequences[i + 2][0] > month_ma200_sequences[i + 2][0]
                ]),
                range(len(month_price_sequences) - 2)
            )]
        )

    def _discover_hold_signal_dates(self) -> numpy.ndarray:
        month_date_sequences, month_ma200_sequences, month_price_sequences = self.__get_monthly_sequences()

        return numpy.array(
            [month_date_sequences[j + 2][0] for j in filter(
                lambda i: all([
                    month_price_sequences[i][-1] < month_price_sequences[i + 1][-1],
                    month_price_sequences[i + 2][0] > month_ma200_sequences[i + 2][0]
                ]),
                range(len(month_price_sequences) - 2)
            )]
        )

    def _discover_trendreversal_signal_dates(self) -> numpy.ndarray:
        prices, dates, ma200 = self._get_moving_average_data(200)
        below_ma200_indexes = numpy.where(prices < ma200)[0]
        return self.__get_first_of_each_window(below_ma200_indexes, dates)

    @staticmethod
    def __get_first_of_each_window(indexes: numpy.ndarray, data: numpy.ndarray) -> numpy.ndarray:
        index_diffs = numpy.diff(indexes)
        # we need to add 1 to every element, since diff has one element less
        sequences = numpy.split(indexes, numpy.where(index_diffs > 1)[0] + 1)
        first_of_windows_indexes = [sequence[0] for sequence in filter(lambda s: s.size > 0, sequences)]
        return data[first_of_windows_indexes]

    def __get_monthly_sequences(self) -> tuple[list[numpy.ndarray], list[numpy.ndarray], list[numpy.ndarray]]:
        prices, dates, ma200 = self._get_moving_average_data(200)
        item_func = numpy.vectorize(lambda d: date(year=d.year, month=d.month, day=1))
        # apply lambda-function to each item of the array
        sequences_of_months = item_func(dates)
        # the result is a sequence of dates, where each item is the first of each month
        # so this date will appear ~20 times
        # now we can calculate the diff of each item
        # and where the diff is greater 0, a new month starts
        # we get the indexes where a new month starts
        month_group_indexes = numpy.where(numpy.diff(sequences_of_months) > timedelta(days=0))[0] + 1
        # and split the array at this position
        # now we have a list of arrays, where each array represents one month
        return numpy.split(dates, month_group_indexes), numpy.split(ma200, month_group_indexes), \
            numpy.split(prices, month_group_indexes)

    def get_most_recent_signal(self) -> tuple[date, SignalType] | None:
        if len(self.signals) > 0:
            return self.signals[-1]
