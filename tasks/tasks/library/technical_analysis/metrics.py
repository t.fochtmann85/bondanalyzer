import datetime
import numpy
import typeguard

from library.technical_analysis.base import BaseCalculator


class MetricCalculator(BaseCalculator):
    @typeguard.typechecked
    def __init__(self, daily_prices: numpy.ndarray, daily_dates: numpy.ndarray):
        super().__init__(daily_prices, daily_dates)

        self._monthly_prices = None
        self._monthly_dates = None
        self._last120_monthly_prices = None

    @property
    def monthly_prices(self) -> numpy.ndarray:
        if self._monthly_prices is None:
            self.__discover_monthly()
        return self._monthly_prices

    @property
    def last120_monthly_prices(self) -> numpy.ndarray:
        if self._last120_monthly_prices is None:
            self._last120_monthly_prices = self.monthly_prices[-120:]
        return self._last120_monthly_prices

    @property
    def monthly_dates(self) -> numpy.ndarray:
        if self._monthly_dates is None:
            self.__discover_monthly()
        return self._monthly_dates

    def __discover_monthly(self) -> None:
        vectorized = numpy.vectorize(lambda d: datetime.date(year=d.year, month=d.month, day=1))
        sequences_of_months = vectorized(self.daily_dates)
        month_group_indexes = numpy.where(numpy.diff(sequences_of_months) > datetime.timedelta())[0]
        self._monthly_prices = self.daily_prices[month_group_indexes]
        self._monthly_dates = self.daily_dates[month_group_indexes]

    def get_month_count(self) -> int:
        return self.monthly_dates.size

    @typeguard.typechecked
    def get_latest_moving_average(self, days: int) -> float:
        assert days > 1, "Moving average should be at least 2."
        days_ = self.daily_prices[-days:]
        return days_.mean()

    def get_latest_price(self) -> float:
        return self.daily_prices[-1]

    def get_loss_ratio(self) -> float:
        # VW
        diff = numpy.diff(self.last120_monthly_prices)
        vw = diff[diff < 0].size / diff.size

        # gDV
        month_ratio = self.last120_monthly_prices[1:] / self.last120_monthly_prices[:-1] - 1
        factors = numpy.where(month_ratio < 0)[0] + 1
        sum_factors = numpy.sum(factors)
        if sum_factors != 0:
            gDV = abs(numpy.sum(factors * month_ratio[month_ratio < 0])) / sum_factors

            return vw * gDV * 100

        return 999

    def get_gain_consistency(self) -> float:
        pos_month_combinations = 0
        total_month_combinations = 0

        it = numpy.nditer(self.last120_monthly_prices, flags=['f_index'])
        for price in it:
            if it.index < self.last120_monthly_prices.size - 1:
                sub_prices = self.last120_monthly_prices[it.index + 1:]
                pos_month_combinations += sub_prices[sub_prices > price].size
                total_month_combinations += sub_prices.size

        return pos_month_combinations / total_month_combinations

    def get_geopac(self) -> float:
        average = self.last120_monthly_prices[:12].mean()
        if average != 0:
            return (self.last120_monthly_prices[-1] / average) ** (12. / self.last120_monthly_prices.size) - 1

        return .0

    def get_all_time_high(self) -> float:
        return self.daily_prices.max()
