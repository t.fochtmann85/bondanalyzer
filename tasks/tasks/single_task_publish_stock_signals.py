import logging

from dotenv import load_dotenv

import system_config
from publisher import SignalPublisher

logging.basicConfig(level=logging.DEBUG)

load_dotenv()

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    cp = system_config.ClientProvider()
    publisher = SignalPublisher(
        cp.get_bondanalyzer_api_client_wrapper(),
    )
    publisher.publish()
