import datetime
import importlib.resources
import json
import logging

from bondanalyzer_schemes.database_schemes import StockRead, StockCreate, StockUpdate, SignalCreate, SignalRead, \
    SignalType, StockFilter
from bondanalyzer_schemes.mixin import StockSignal
from .bondanalyzer_api_client_wrapper_interface import BondanalyzerApiClientWrapperInterface

logger = logging.getLogger(__name__)


class BondanalyzerApiClientWrapperDummy(BondanalyzerApiClientWrapperInterface):

    def __init__(self):
        logger.debug("Using dummy for bondanalyzer REST-API client.")
        super().__init__()
        with importlib.resources.path('resources', 'initial_stocks.json') as path:
            with path.open() as f:
                self.initial_stocks = json.load(f)

    def get_stocks(self, inner: bool = True) -> list[StockRead]:
        return [StockRead(**item, id=1) for item in self.initial_stocks]

    def filter_stocks(self, stock_filter: StockFilter) -> list[StockRead]:
        return self.get_stocks()

    def get_stock(self, ticker: str) -> StockRead | None:
        for stock in self.get_stocks():
            if stock.ticker == ticker:
                return stock

    def create_stock(self, stock: StockCreate) -> StockRead:
        return self.get_stocks()[0]

    def update_stock(self, ticker: str, stock_update: StockUpdate) -> StockRead:
        return self.get_stocks()[0]

    def delete_stock(self, ticker: str) -> None:
        pass

    def create_stock_signal(self, ticker: str, signal: SignalCreate) -> SignalRead | None:
        return SignalRead(date=datetime.date.today(), signal_type=SignalType.BUY)

    def get_signals(self) -> list[StockSignal]:
        return [StockSignal(signal=SignalRead(date=datetime.date.today(), signal_type=SignalType.BUY),
                            stock=self.get_stocks()[0])]
