import logging
import os

import requests
import requests_unixsocket

if int(os.environ.get("MONKEYPATCH_UNIXSOCKET_REQUESTS", "1")):
    requests_unixsocket.monkeypatch()

logger = logging.getLogger(__file__)


class BondanalyzerApiClient:
    def __init__(self):
        self._base_url = None

    @property
    def base_url(self):
        if self._base_url is None:
            self._base_url = os.environ["BONDANALYZER_API_URL"]
        return self._base_url

    def get_stocks(self, inner: bool = True) -> requests.Response:
        return self._get_request_response("stock", params={"inner": inner}, need_auth=False)

    def filter_stocks(self, stock_filter: dict) -> requests.Response:
        return self._get_request_response("stock/filter", data=stock_filter, method="POST", need_auth=False)

    def get_stock(self, ticker: str) -> requests.Response:
        return self._get_request_response(f"stock/{ticker}", need_auth=False)

    def create_stock(self, stock: dict) -> requests.Response:
        return self._get_request_response("stock", data=stock, method="POST")

    def update_stock(self, ticker: str, stock_update: dict) -> requests.Response:
        return self._get_request_response(f"stock/{ticker}", data=stock_update, method="PUT")

    def delete_stock(self, ticker: str) -> requests.Response:
        return self._get_request_response(f"stock/{ticker}", method="DELETE")

    def create_stock_signal(self, ticker: str, signal: dict) -> requests.Response:
        return self._get_request_response(f"stock/{ticker}/signal", data=signal, method="POST")

    def get_signals(self) -> requests.Response:
        return self._get_request_response("signal", need_auth=False)

    def _get_request_response(self, rel_url: str, data=None, params=None, method: str = "GET",
                              need_auth: bool = True) -> requests.Response:
        url = f"{self.base_url}/api/v1/{rel_url}"
        logger.debug(f"Call REST-API {url} with {params}")

        headers = dict()
        if need_auth:
            headers['Authorization'] = f"Bearer {os.environ['BONDANALYZER_API_KEY']}"

        return requests.request(method, url, data=data, params=params, headers=headers)
