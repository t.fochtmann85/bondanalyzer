import logging

import requests

from bondanalyzer_schemes.database_schemes import StockRead, StockCreate, StockUpdate, SignalRead, \
    SignalCreate, StockFilter
from bondanalyzer_schemes.mixin import StockSignal
from .bondanalyzer_api_client import BondanalyzerApiClient
from .bondanalyzer_api_client_wrapper_interface import BondanalyzerApiClientWrapperInterface

logger = logging.getLogger(__name__)


class BondanalyzerApiClientWrapper(BondanalyzerApiClientWrapperInterface):

    def __init__(self):
        super().__init__()
        self._ap = BondanalyzerApiClient()

    def get_stocks(self, inner: bool = True) -> list[StockRead]:
        resp = self._ap.get_stocks(inner)
        self._check_response(resp)
        return [StockRead(**item) for item in resp.json()]

    def filter_stocks(self, stock_filter: StockFilter) -> list[StockRead]:
        resp = self._ap.filter_stocks(stock_filter.json())
        self._check_response(resp)
        return [StockRead(**item) for item in resp.json()]

    def get_stock(self, ticker: str) -> StockRead | None:
        resp = self._ap.get_stock(ticker)
        self._check_response(resp)
        return StockRead(**resp.json())
        # TODO: what if not exist?

    def create_stock(self, stock: StockCreate) -> StockRead:
        resp = self._ap.create_stock(stock.json())
        self._check_response(resp)
        return StockRead(**resp.json())

    def update_stock(self, ticker: str, stock_update: StockUpdate) -> StockRead:
        resp = self._ap.update_stock(ticker, stock_update.json())
        self._check_response(resp)
        return StockRead(**resp.json())

    def delete_stock(self, ticker: str) -> None:
        resp = self._ap.delete_stock(ticker)
        self._check_response(resp)

    def create_stock_signal(self, ticker: str, signal: SignalCreate) -> SignalRead | None:
        resp = self._ap.create_stock_signal(ticker, signal.json())
        self._check_response(resp)
        if resp.status_code == 204:
            return None
        else:
            return SignalRead(**resp.json())

    def get_signals(self) -> list[StockSignal]:
        resp = self._ap.get_signals()
        self._check_response(resp)
        return [StockSignal(**item) for item in resp.json()]

    def _check_response(self, resp: requests.Response) -> None:
        if resp.status_code >= 400:
            raise Exception(f"REST-API stated an error ({resp.status_code}):\n{resp.text}")
        logger.debug(f"REST-API returned {resp.status_code}.")
