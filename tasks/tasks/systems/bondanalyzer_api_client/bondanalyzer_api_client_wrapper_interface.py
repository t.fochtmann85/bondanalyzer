import logging
from abc import abstractmethod

from bondanalyzer_schemes.database_schemes import StockRead, StockCreate, StockUpdate, SignalCreate, SignalRead, \
    StockFilter
from bondanalyzer_schemes.mixin import StockSignal

logger = logging.getLogger(__name__)


class BondanalyzerApiClientWrapperInterface:
    @abstractmethod
    def get_stocks(self, inner: bool = True) -> list[StockRead]:
        pass

    @abstractmethod
    def filter_stocks(self, stock_filter: StockFilter) -> list[StockRead]:
        pass

    @abstractmethod
    def get_stock(self, ticker: str) -> StockRead:
        pass

    @abstractmethod
    def create_stock(self, stock: StockCreate) -> StockRead:
        pass

    @abstractmethod
    def update_stock(self, ticker: str, stock_update: StockUpdate) -> StockRead:
        pass

    @abstractmethod
    def delete_stock(self, ticker: str) -> None:
        pass

    @abstractmethod
    def create_stock_signal(self, ticker: str, signal: SignalCreate) -> SignalRead | None:
        pass

    @abstractmethod
    def get_signals(self) -> list[StockSignal]:
        pass
