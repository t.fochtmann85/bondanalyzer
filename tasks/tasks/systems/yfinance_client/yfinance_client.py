import logging
import re

import pandas
import yfinance

from systems.yfinance_client.yfinance_client_interface import YfinanceClientInterface

logger = logging.getLogger(__name__)


class YfinanceClient(YfinanceClientInterface):
    ISIN_PATTERN = re.compile(r"^[A-Z]{2}[A-Z0-9]{9}\d$", flags=re.IGNORECASE)

    def get_ticker_prices(self, tickers: list[str], *args, **kwargs) -> pandas.DataFrame:
        return yfinance.download(tickers, *args, **kwargs)

    def get_ticker_info(self, ticker: str) -> dict:
        return yfinance.Ticker(ticker).fast_info

    def get_isin_by_ticker(self, ticker: str) -> str | None:
        try:
            isin = yfinance.Ticker(ticker).isin
        except Exception as e:
            logger.error(f"While requesting ISIN for ticker {ticker!r} the following exception occurred:\n{e}")
            return None

        if not isinstance(isin, str):
            return None

        isin = self._get_cleaned_isin(isin)
        if self.matches_isin_pattern(isin):
            return isin

        return None

    @staticmethod
    def _get_cleaned_isin(isin: str) -> str:
        return re.sub(r"[^a-z0-9]", "", isin, flags=re.IGNORECASE)

    @classmethod
    def matches_isin_pattern(cls, isin: str) -> bool:
        return cls.ISIN_PATTERN.match(isin) is not None
