import importlib.resources
import logging

import pandas

from systems.yfinance_client.yfinance_client_interface import YfinanceClientInterface

logger = logging.getLogger(__name__)


class YfinanceClientDummy(YfinanceClientInterface):

    def __init__(self):
        logger.debug("Using dummy for yfinance client.")
        with importlib.resources.path("resources", "apc_nvd_raw.csv") as path:
            self._df = pandas.read_csv(path, index_col=0, header=[0, 1])
            self._df.index = pandas.to_datetime(self._df.index)

    def get_ticker_prices(self, tickers: list[str], *args, **kwargs) -> pandas.DataFrame:
        return self._df

    def get_ticker_info(self, ticker: str) -> dict:
        return {'marketCap': 42}

    def get_isin_by_ticker(self, ticker: str) -> str | None:
        return 'DE243554363456'

    @classmethod
    def matches_isin_pattern(cls, isin: str) -> bool:
        return True
