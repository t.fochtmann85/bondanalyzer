from abc import abstractmethod

import pandas


class YfinanceClientInterface:
    @abstractmethod
    def get_ticker_prices(self, tickers: list[str], *args, **kwargs) -> pandas.DataFrame:
        pass

    @abstractmethod
    def get_ticker_info(self, ticker: str) -> dict:
        pass

    @abstractmethod
    def get_isin_by_ticker(self, ticker: str) -> str | None:
        pass

    @classmethod
    @abstractmethod
    def matches_isin_pattern(cls, isin: str) -> bool:
        pass
