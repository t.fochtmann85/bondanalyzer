from abc import abstractmethod


class TwelvedataClientInterface:
    @abstractmethod
    def get_exchanges(self) -> list[dict]:
        pass

    @abstractmethod
    def get_stocks_of_exchange(self, exchange_code: str) -> list[dict]:
        pass
