import importlib.resources
import json

from systems.twelve_data_client.twelve_data_client_interface import TwelvedataClientInterface


class TwelvedataClientDummy(TwelvedataClientInterface):
    def get_exchanges(self) -> list[dict]:
        with importlib.resources.path("resources", "exchanges.json") as path:
            with path.open() as f:
                return json.load(f)['data']

    def get_stocks_of_exchange(self, exchange_code: str) -> list[dict]:
        with importlib.resources.path("resources", f"stock_exchange_{exchange_code}.json") as path:
            with path.open() as f:
                return json.load(f)['data']
