from enum import Enum

from pydantic import BaseModel


class Exchange(BaseModel):
    code: str
    country: str
    name: str
    timezone: str


class StockType(Enum):
    EQUITY = "EQUITY"
    COMMON = "Common"
    COMMON_STOCK = "Common Stock"
    AMERICAN_DEPOSITARY_RECEIPT = "American Depositary Receipt"
    GLOBAL_DEPOSITARY_RECEIPT = "Global Depositary Receipt"
    REAL_ESTATE_INV_TRUST = "Real Estate Investment Trust (REIT)"
    REIT = "REIT"
    UNIT = "Unit"
    GDR = "GDR"
    CLOSED_END_FUND = "Closed-end Fund"
    PREFERRED_STOCK = "Preferred Stock"
    ETF = "ETF"
    NY_REG_SHARES = "New York Registered Shares"


class Stock(BaseModel):
    country: str  # Germany
    currency: str  # EUR
    exchange: str  # XDUS
    name: str  # Gofore Oyj
    symbol: str  # 1E2
    type: StockType
