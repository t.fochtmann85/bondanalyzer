import logging
from typing import Iterable

from systems.twelve_data_client.models import Exchange, Stock
from systems.twelve_data_client.twelve_data_client_interface import TwelvedataClientInterface

logger = logging.getLogger(__name__)


class TwelvedataClientWrapper:
    def __init__(self, client: TwelvedataClientInterface):
        self._client = client

    def get_exchanges(self) -> list[Exchange]:
        return [Exchange(**item) for item in self._client.get_exchanges()]

    def get_stocks_of_exchange(self, exchange_code: str) -> Iterable[Stock]:
        for item in self._client.get_stocks_of_exchange(exchange_code):
            try:
                yield Stock(**item)
            except Exception as exc:
                logger.error(
                    f"While creating a pydantic model {Stock.__name__} from\n"
                    f"{item}\n"
                    "the following error occurred:\n"
                    f"{exc}"
                )
