import os

import requests
import time

from systems.twelve_data_client.twelve_data_client_interface import TwelvedataClientInterface


class TwelvedataClient(TwelvedataClientInterface):
    URL = "https://twelve-data1.p.rapidapi.com"

    def __init__(self) -> None:
        super().__init__()
        self._headers = None
        self._last_call = 0

    @property
    def headers(self) -> dict:
        if self._headers is None:
            self._headers = {
                'x-rapidapi-host': "twelve-data1.p.rapidapi.com",
                'x-rapidapi-key': os.environ["RAPID_API_KEY"]
            }
        return self._headers

    def get_exchanges(self) -> list[dict]:
        return self._request("exchanges", {})["data"]

    def get_stocks_of_exchange(self, exchange_code: str) -> list[dict]:
        return self._request("stocks", {"exchange": exchange_code})["data"]

    def _request(self, rel_url: str, params: dict) -> dict | list:
        # wait at least 12s after last call
        time.sleep(max(0, int(self._last_call + 12 - time.time())))
        resp = requests.get(
            f"{self.URL}/{rel_url}",
            headers=self.headers,
            params={**params, "format": "json"}
        )
        self._last_call = time.time()
        return resp.json()
