import os
import re

import requests
import time

from systems.alpha_vantage.alpha_vantage_client_interface import AlphaVantageClientInterface


class AlphaVantageClient(AlphaVantageClientInterface):
    URL = "https://alpha-vantage.p.rapidapi.com/query"

    def __init__(self) -> None:
        super().__init__()
        self._headers = None
        self._last_call = 0

    @property
    def headers(self) -> dict:
        if self._headers is None:
            self._headers = {
                'x-rapidapi-host': "alpha-vantage.p.rapidapi.com",
                'x-rapidapi-key': os.environ["RAPID_API_KEY"]
            }
        return self._headers

    def _request(self, params: dict) -> dict | list:
        # wait at least 12s after last call
        time.sleep(max(0, int(self._last_call + 12 - time.time())))
        resp = requests.get(
            self.URL,
            headers=self.headers,
            params=params
        )
        self._last_call = time.time()
        return resp.json()

    def get_symbol_by_stock_ticker(self, ticker: str) -> list:
        return self._request({
            "keywords": ticker,
            "function": "SYMBOL_SEARCH",
            "datatype": "json"
        }).get("bestMatches", [])

    def get_symbol_by_stock_name(self, name: str) -> list:
        return self._request({
            "keywords": re.sub(r"[^\w ]", "", name),
            "function": "SYMBOL_SEARCH",
            "datatype": "json"
        }).get("bestMatches", [])

    def get_fundamental(self, symbol: str) -> dict:
        return self._request(
            {
                "symbol": symbol,
                "function": "OVERVIEW",
                "datatype": "json"
            }
        )
    # TODO: wrapper to cache information
