from abc import abstractmethod


class AlphaVantageClientInterface:
    @abstractmethod
    def get_symbol_by_stock_ticker(self, ticker: str) -> list:
        pass

    @abstractmethod
    def get_symbol_by_stock_name(self, name: str) -> list:
        pass

    @abstractmethod
    def get_fundamental(self, symbol: str) -> dict:
        pass
