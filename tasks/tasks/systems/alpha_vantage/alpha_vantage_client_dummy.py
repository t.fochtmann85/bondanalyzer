import logging

from systems.alpha_vantage.alpha_vantage_client_interface import AlphaVantageClientInterface

logger = logging.getLogger(__name__)


class AlphaVantageClientDummy(AlphaVantageClientInterface):
    def __init__(self) -> None:
        logger.debug("Using dummy for alpha vantage REST-API client.")
        super().__init__()

    def get_symbol_by_stock_ticker(self, ticker: str) -> list:
        return [{"1. symbol": ticker.replace(".F", ".FRK")}]

    def get_symbol_by_stock_name(self, name: str) -> list:
        return [{"1. symbol": "APC"}]

    def get_fundamental(self, symbol: str) -> dict:
        return {'MarketCapitalization': 42}
