import importlib.resources
import logging

import yaml

from bondanalyzer_schemes.database_schemes import StockRead
from systems.alpha_vantage.alpha_vantage_client_interface import AlphaVantageClientInterface

logger = logging.getLogger(__name__)


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class AlphaVantageSymbolOracle(metaclass=Singleton):
    FILE = "alpha_vantage_symbols.yaml"

    def __init__(self, avc: AlphaVantageClientInterface):
        self._translation = dict()
        try:
            with importlib.resources.path("resources", self.FILE) as path:
                with path.open() as f:
                    self._translation = yaml.safe_load(f)
        except FileNotFoundError:
            pass
        self._avc = avc

    def _save(self) -> None:
        with importlib.resources.path("resources", self.FILE) as path:
            with path.open(mode="w") as f:
                yaml.dump(self._translation, f)

    def get_symbol_from_stock(self, stock: StockRead) -> str | None:
        ticker = stock.ticker.replace(".F", ".FRK").replace(".DE", ".DEX")
        if ticker in self._translation:
            return self._translation[ticker]

        symbol_findings = self._avc.get_symbol_by_stock_ticker(ticker)
        if len(symbol_findings) == 0:
            symbol_findings = self._avc.get_symbol_by_stock_name(stock.name)

        if len(symbol_findings) > 0:
            symbol = symbol_findings[0].get("1. symbol")
            self._translation[ticker] = symbol
            self._save()
            return self._translation[ticker]

        logger.warning(f"Unable to discover stock symbol for ticker {stock.ticker!r} from alpha vantage.")
        return None
