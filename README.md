# What is this ?

To be documented

# Installation Guide

## SSH

```
pi@raspberrypi:~ $ sudo apt-get install ssh
pi@raspberrypi:~ $ sudo /etc/init.d/ssh start
pi@raspberrypi:~ $ sudo update-rc.d ssh defaults
```

- https://www.heise.de/tipps-tricks/Raspberry-Pi-SSH-einrichten-so-geht-s-4190645.html

## PostgreSQL

Create for databases bondanalyzer (user: bondanalyzer_rest_api) and bondanalyzer_test (user: test_user)

### Installation

```
pi@raspberrypi:~ $ sudo apt update
pi@raspberrypi:~ $ sudo apt full-upgrade
pi@raspberrypi:~ $ sudo apt install postgresql postgresql-contrib
```

### Setup for remote access

```
pi@raspberrypi:~ $ sudo -su postgres
postgres@raspberrypi:~$ psql
postgres=# SHOW hba_file;
> /etc/postgresql/15/main/pg_hba.conf
```

Alter hba_conf
```
postgres=# exit
postgres@raspberrypi:~$ exit
pi@raspberrypi:~ $ sudo nano /etc/postgresql/15/main/pg_hba.conf
# add line at IPv4:   host all all 192.169.178.31/32 trust
```

Alter postgresql.conf
```
pi@raspberrypi:~ $ sudo nano /etc/postgresql/15/main/postgresql.conf
# alter line listen_addresses; uncomment, change 'localhost' to '*'
```

Restart postgresql
```
pi@raspberrypi:~ $ sudo /etc/init.d/postgresql restart
```

### Create user

```
pi@raspberrypi:~ $ sudo -su postgres
postgres@raspberrypi:~$ psql
>>> in case of 'could not change directory to "/home/pi": Permission denied': > sudo chmod og+rX /home /home/pi
postgres=# \l
postgres=# exit
postgres@raspberrypi:~$ createuser --interactive
# user: ba_rest_api; superuser: yes
postgres@raspberrypi:~$ createuser --interactive
# user: ba_rest_api_test; superuser: yes
postgres@raspberrypi:~$ psql
postgres=# \du
postgres=# exit
```

### Create database

```
postgres@raspberrypi:~$ createdb bondanalyzer
postgres@raspberrypi:~$ createdb bondanalyzer_test
```

### Give test user access to the database

```
postgres@raspberrypi:~$ psql
postgres=# alter user ba_rest_api_test with encrypted password '***';
postgres=# grant all privileges on database bondanalyzer_test to ba_rest_api_test;
postgres=# \l
```

### Test the remote connection

On the windows laptop in the repo workspace, set the .env file in the folder rest-api/tests
```
POSTGRES_DB=bondanalyzer_test
POSTGRES_PASSWORD=***
POSTGRES_PORT=5432
POSTGRES_SERVER=192.168.178.36
POSTGRES_USER=ba_rest_api_test
BONDANALYZER_ADMIN=hubba@bubba.net
```
Make sure they work before going forward!

### Give technical user access to the database

```
postgres@raspberrypi:~$ psql
postgres=# alter user ba_rest_api with encrypted password 'ozmIMnP90MQhlXqYF1Im';
postgres=# grant all privileges on database bondanalyzer to ba_rest_api;
postgres=# \l
postgres=# exit
postgres@raspberrypi:~$ exit
```

### test local access

```
pi@raspberrypi:~ $ pg_isready -d bondanalyzer -h localhost -p 5432 -U ba_rest_api
```

## Poetry

```
pi@raspberrypi:~ $ curl -sSL https://install.python-poetry.org | python3.11 -
```

- https://python-poetry.org/docs/#installation

## Clone

```
pi@raspberrypi:~ $ mkdir dev
pi@raspberrypi:~ $ cd dev
pi@raspberrypi:~/dev $ git clone https://gitlab.com/t.fochtmann85/bondanalyzer.git
```

## Install Dependencies for REST-API

Not via SSH, since a keyring is used (asks for password) and GUI dialog will appear!

```
pi@raspberrypi:~/dev $ sudo apt-get install libpq-dev
pi@raspberrypi:~/dev $ cd bondanalyzer/rest-api
pi@raspberrypi:~/dev/bondanalyzer/rest-api $ poetry install
# /home/pi/.cache/pypoetry/virtualenvs/rest-api-ke4vd0UY-py3.11a
```

## Create `.env` file

```
POSTGRES_DB=bondanalyzer
POSTGRES_PASSWORD=***
POSTGRES_PORT=5432
POSTGRES_SERVER=localhost
POSTGRES_USER=ba_rest_api
BONDANALYZER_ADMIN=bondanalyzer@gmail.com
MAIL_SERVER=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=bondanalyzer@gmail.com
MAIL_PASSWORD=***
```

## Create log folder

```
pi@raspberrypi:~/dev $ mkdir bondanalyzer-log
```

## add keys file for api keys

Mind `rest-api/rest_api/__api_key_gen.py`

```
pi@raspberrypi:~/dev/bondanalyzer/rest-api $ cd resources
pi@raspberrypi:~/dev/bondanalyzer/rest-api/resources $ nano keys
```

```
---
- APIKEY1
- APIKEY2
```

### Try and Error

Following must run successfully!

```
pi@raspberrypi:~/dev/bondanalyzer/rest-api $ cd rest_api
pi@raspberrypi:~/dev/bondanalyzer/rest-api $ /home/pi/.cache/pypoetry/virtualenvs/rest-api-BJmHXiPG-py3.11/bin/uvicorn main:app --reload --port 8099
```

## NGINX 

### Installation

```
pi@raspberrypi:~ $ sudo apt remove apache2
pi@raspberrypi:~ $ sudo apt install nginx
pi@raspberrypi:~ $ sudo systemctl start nginx
pi@raspberrypi:~ $ sudo systemctl enable nginx
```

### configuration

```
pi@raspberrypi:~ $ sudo nano /etc/nginx/sites-available/bondanalyzer
```

```
server {
    listen 80;
    location / {
        include proxy_params;
        proxy_pass http://localhost:8099;
    }
}
```

An additional service needs additional settings

```
server {
    listen 80;
    location /app {
        include proxy_params;
        proxy_pass http://localhost:3000;
    }
    location /api {
        include proxy_params;
        proxy_pass http://localhost:8099;
    }
}
```

create symbolic link at `/etc/nginx/sites-enabled`

```
pi@raspberrypi:~ $ sudo ln -s /etc/nginx/sites-available/bondanalyzer /etc/nginx/sites-enabled/bondanalyzer
```

### Test config

```
sudo nginx -t
```

### Reload NGINX

```
sudo systemctl restart nginx
sudo systemctl reload nginx
```

### NGINX Troubleshoot

- If the service configuration at `/etc/nginx/sites-available/bondanalyzer` seems not to work, 
  check `/etc/nginx/nginx.conf` for `include` statements; there might be a file somewhere
  (`/etc/nginx/sites-enabled/default`) that listens on port 80 as well and overwrites our setting

### Start, Stop, Status of service

```
sudo systemctl start bondanalyzer_rest_api.service
sudo systemctl status bondanalyzer_rest_api.service
sudo systemctl stop bondanalyzer_rest_api.service
```

### Reload after service configuration change

```
sudo systemctl daemon-reload
```

### Logs of NGINX stored at `/var/log/nginx`

## Install Dependencies for tasks

Not via SSH, since a keyring is used (asks for password) and GUI dialog will appear!

```
pi@raspberrypi:~/dev $ cd bondanalyzer/tasks
pi@raspberrypi:~/dev/bondanalyzer/tasks $ poetry install
# /home/pi/.cache/pypoetry/virtualenvs/tasks-W-H9Ro9y-py3.11
```

## Create `.env` file

```
LOG_LEVEL=DEBUG
RAPID_API_KEY=***
BONDANALYZER_API_URL=http://localhost:8099
BONDANALYZER_API_KEY=***
MAIL_SERVER=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=bondanalyzer@gmail.com
MAIL_PASSWORD=***
```

## Initialize alembic

First migration script

```
set POSTGRES_USER=ba_rest_api
set POSTGRES_PASSWORD=***
set POSTGRES_SERVER=192.168.178.***
set POSTGRES_DB=bondanalyzer
set POSTGRES_PORT=5432
poetry run alembic revision -m "initial migration script"
```

Make alembic versioning available in the database

```
set POSTGRES_USER=ba_rest_api
set POSTGRES_PASSWORD=***
set POSTGRES_SERVER=192.168.178.***
set POSTGRES_DB=bondanalyzer
set POSTGRES_PORT=5432
poetry run alembic upgrade head
```

Do it for `test` database as well!

Any future migration:

```
poetry run alembic revision --autogenerate -m "add all-time-high"
```

Take a look at the script and delete stuff you don't need.

```
poetry run alembic upgrade head
```


## Add initial stocks

```
pi@raspberrypi:~/dev/bondanalyzer/tasks/tasks $ poetry run python scripts/initialize_database.py
pi@raspberrypi:~ $ curl http://localhost:8099/api/v1/stock
```

## Creating the REST-API service

```
pi@raspberrypi:~ $ sudo nano /etc/systemd/system/bondanalyzer_rest_api.service
```

```
[Unit]
Description=uvicorn instance to serve bondanalyzer's REST-API
After=network.target

[Service]
User=pi
Group=www-data
WorkingDirectory=/home/pi/dev/bondanalyzer/rest-api/rest_api
Environment="PATH=/home/pi/.cache/pypoetry/virtualenvs/rest-api-ke4vd0UY-py3.11/bin"
ExecStart=/home/pi/.cache/pypoetry/virtualenvs/rest-api-ke4vd0UY-py3.11/bin/uvicorn main:app --reload --port 8099

[Install]
WantedBy=multi-user.target
```

```
pi@raspberrypi:~ $ sudo systemctl start bondanalyzer_rest_api
pi@raspberrypi:~ $ sudo systemctl enable bondanalyzer_rest_api
```

- https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6


### Cron jobs

```
crontab -e
```

Add a line for each task

```
# m h  dom mon dow   command
30 21  *  *  1-5 /home/pi/.cache/pypoetry/virtualenvs/tasks-W-H9Ro9y-py3.11/bin/python /home/pi/dev/bondanalyzer/tasks/tasks/task_update_stocks_and_notify.py
 0  2  *  *    6 /home/pi/.cache/pypoetry/virtualenvs/tasks-W-H9Ro9y-py3.11/bin/python /home/pi/dev/bondanalyzer/tasks/tasks/task_update_foreign_stocks.py
```

```
sudo nano /etc/rsyslog.conf
```

## WebApp


```
cd webapp
npm install
```

```
pi@raspberrypi:~ $ sudo nano /etc/systemd/system/bondanalyzer_webapp.service
```

```
[Unit]
Description=Webapp for bondanalyzer' (nextjs)
After=network.target

[Service]
User=pi
Group=www-data
Restart=on-failure
RestartSec=10
WorkingDirectory=/home/pi/dev/bondanalyzer/webapp
ExecStartPre=npm install
ExecStartPre=/npm run build
ExecStart=npm run start

[Install]
WantedBy=multi-user.target
```

```
pi@raspberrypi:~ $ sudo systemctl start bondanalyzer_webapp
pi@raspberrypi:~ $ sudo systemctl enable bondanalyzer_webapp
```

In case a (file) change of the service is necessary

```
sudo systemctl daemon-reload
```


## DynDNS

- https://gist.github.com/PhilMurwin/22ac3674825daa8e22f265178d69c084

```
pi@raspberrypi:/etc/nginx $ sudo apt-get install ddclient
```

File at `/etc/ddclient.conf` must look like:

```
# Configuration file for ddclient generated by debconf
#
# /etc/ddclient.conf

protocol=dyndns2
ssl=yes
use=if
usev6=ifv6, if=wlan0
ipv6=yes
ipv4=no
server=ipv6.dynv6.com
login=none
password='****'
bondanalyzer.dynv6.net
```