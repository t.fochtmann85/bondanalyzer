import { render, screen } from '@testing-library/react'
import StocksOverview from '../../pages/index'

describe('StocksOverview', () => {
  it('Sorry statement since stock list is empty', () => {
    const { container } = render(<StocksOverview apiData={new Array()}/>)

    const heading = screen.getByText("Sorry, didn't receive any information from REST-API 😥");

    expect(heading).toBeInTheDocument()

    expect(container).toMatchSnapshot()
  })
})