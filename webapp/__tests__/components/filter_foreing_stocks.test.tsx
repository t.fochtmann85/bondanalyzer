import { act, fireEvent, render, screen } from "@testing-library/react";
import { useState } from "react";
import { BaseFilterSettings } from "webapp/components/filter_foreign_stocks";
import { SearchFilter } from "webapp/components/types";

const initSearchResultSpecifics = {
  count: 0,
  sectors: new Array(),
  market_cap_min: 0,
  market_cap_max: 9999 * 1e9,
  months_min: 0,
  months_max: 120,
  score_min: -999,
  score_max: 999,
  geopac_min: 0,
  geopac_max: 99999,
  gain_consistency_min: 0,
  gain_consistency_max: 100,
  loss_ratio_min: 0,
  loss_ratio_max: 999,
  trend_min: -100,
  trend_max: 100,
};

const initSearchFilter = {
  inner: false,
};

describe("BaseFilterSettings", () => {
  it.each([
    ["market_cap_min", "0.05", 0.05 * 1e9],
    ["market_cap_max", "0.65", 0.65 * 1e9],
    ["score_min", "50", 50],
    ["score_max", "100", 100],
    ["geopac_min", "90", 0.9],
    ["geopac_max", "144", 1.44],
    ["gain_consistency_min", "85", 0.85],
    ["gain_consistency_max", "99", 0.99],
    ["loss_ratio_min", "1.0", 1],
    ["loss_ratio_max", "2.5", 2.5],
    ["months_min", "72", 72],
    ["months_max", "120", 120],
    ["trend_min", "-20.7", -20.7],
    ["trend_max", "15.3", 15.3],
  ])(
    "Test %p",
    (name: string, valueToSet: string, expectedFilterValue: number) => {
      let searchSpecifics = initSearchFilter;
      const setFunc = (specs: SearchFilter) => {
        searchSpecifics = specs;
      };

      const { container } = render(
        <BaseFilterSettings
          initSearchResultSpecs={initSearchResultSpecifics}
          searchFilter={searchSpecifics}
          setSearchSpecifics={setFunc}
        />
      );

      // get input node
      const inputElem = container.querySelector('input[name="' + name + '"]');
      expect(inputElem != null).toBeTruthy();

      // change value of input
      fireEvent.change(inputElem as HTMLInputElement, {
        target: { value: valueToSet },
      });

      // check value has changed
      expect(screen.getByDisplayValue(valueToSet)).toBe(inputElem);

      // change unchanged filter
      expect(searchSpecifics).toStrictEqual({ inner: false });

      // get button to filter
      const filterButton = container.querySelector("button");
      expect(filterButton != null).toBeTruthy();

      // click Button
      fireEvent.click(filterButton as HTMLButtonElement);

      // filter should be updated
      expect(searchSpecifics).toStrictEqual({
        inner: false,
        [name]: expectedFilterValue,
      });

      // test reset
      fireEvent.change(inputElem as HTMLInputElement, {
        target: { value: "" },
      });

      // click Button
      fireEvent.click(filterButton as HTMLButtonElement);

      // change unchanged filter
      expect(searchSpecifics).toStrictEqual({ inner: false });
    }
  );
});
