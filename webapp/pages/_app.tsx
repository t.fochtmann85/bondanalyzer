import type { AppProps } from "next/app";
import "webapp/styles/globals.scss";
import Layout from "../components/layout";
import Head from "next/head";

export default function BondanalyzerWebApp({ Component, pageProps }: AppProps) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}
