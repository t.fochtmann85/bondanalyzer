import { useState } from "react";
import useSWR from "swr";
import fetcher from "webapp/components/fetcher";
import { BaseFilterSettings } from "webapp/components/filter_foreign_stocks";
import { ForeignStocksTable } from "webapp/components/table_foreign_stocks";
import { SearchFilter, SearchResultSpecifics } from "webapp/components/types";

export default function ForeignStocksOverview({
  initSearchResultSpecifics,
  initSearchFilter,
}: {
  initSearchResultSpecifics: SearchResultSpecifics;
  initSearchFilter: SearchFilter;
}) {
  let tableNode = undefined;

  // (1) create to pass into base filter settings - this is the search filter
  const [searchSpecifics, setSearchSpecifics] = useState(initSearchFilter);

  // (2) build filter settings section
  const filterSettingsNode = (
    <BaseFilterSettings
      initSearchResultSpecs={initSearchResultSpecifics}
      searchFilter={searchSpecifics}
      setSearchSpecifics={setSearchSpecifics}
    />
  );

  // (3) get result specs with current filter
  const searchResultSpecs: SearchResultSpecifics =
    GetForeignStockSpecifics(searchSpecifics);

  let notes = undefined;

  if (searchResultSpecs.count > 100) {
    notes = (
      <div className="notification is-info">
        Your current filter results in {initSearchResultSpecifics.count} stocks.
        Please rearrange your filter settings to have a maximum of 100 stocks.
      </div>
    );
  } else {
    tableNode = <ForeignStocksTable searchSpecifics={searchSpecifics} />;
  }

  if (notes) {
    notes = (<div className="p-5">{notes}</div>);
  }

  return (
    <div>
      <div className="p-5">{filterSettingsNode}</div>
      {notes}
      <div className="p-5">{tableNode}</div>
    </div>
  );
}

function GetForeignStockSpecifics(
  searchSpecifics: SearchFilter
): SearchResultSpecifics {
  const { data, error, isLoading } = useSWR<SearchResultSpecifics, Error>(
    {
      input: "http://bondanalyzer.dynv6.net/api/v1/stock/search/specifics",
      init: {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(searchSpecifics),
      },
    },
    fetcher,
    {
      revalidateIfStale: false,
      revalidateOnFocus: false,
    }
  );
  if (data) return data;
  else return defaultSearchResultSpecifics;
}

const defaultSearchResultSpecifics = {
  count: 0,
  sectors: new Array(),
  market_cap_min: 0,
  market_cap_max: 9999 * 1e9,
  months_min: 0,
  months_max: 120,
  score_min: -999,
  score_max: 999,
  geopac_min: 0,
  geopac_max: 99999,
  gain_consistency_min: 0,
  gain_consistency_max: 100,
  loss_ratio_min: 0,
  loss_ratio_max: 999,
  trend_min: -100,
  trend_max: 100,
};

export async function getServerSideProps() {
  const initSearchFilter = { inner: false };
  try {
    // use ip address due to DNS lookup issue
    // https://github.com/nodejs/node/issues/40702#issuecomment-958143154
    const res = await fetch(
      "http://bondanalyzer.dynv6.net/api/v1/stock/search/specifics",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ inner: false }),
      }
    );
    const initSearchResultSpecifics: SearchResultSpecifics | undefined =
      await res.json();
    if (initSearchResultSpecifics) {
      return { props: { initSearchResultSpecifics, initSearchFilter } };
    }
  } catch (Error) {}
  return { props: { initSearchResultSpecifics: defaultSearchResultSpecifics, initSearchFilter } };
}
