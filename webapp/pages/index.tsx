import { useMemo } from "react";
import { getFrontendDataFromRawStockSignal } from "webapp/components/converter";
import { StockDetail } from "webapp/components/stock_detail";
import { tableHeaderStocks } from "webapp/components/table_headers";
import { StocksTable } from "webapp/components/table_stocks";
import {
  FrontendStockSignal,
  Stock,
  StockSignal,
} from "webapp/components/types";

export default function StocksOverview({
  apiData,
}: {
  apiData: StockSignal[];
}) {
  // const { stockSignals, isLoading, isError } = getStockSignals();
  const columns = useMemo(() => {
    return tableHeaderStocks;
  }, []);
  const data = useMemo(
    () =>
      apiData.map((stockSignal) =>
        getFrontendDataFromRawStockSignal(stockSignal)
      ),
    []
  );

  if (apiData == null || apiData.length == 0) {
    return (
      <div className="notification is-danger">
        Sorry, didn&apos;t receive any information from REST-API 😥
      </div>
    );
  }

  return (
    <div>
      <StocksTable columns={columns} data={data} />
      {(apiData as StockSignal[]).map((stockSignal: StockSignal) => (
        <StockDetail stockSignal={stockSignal} />
      ))}
    </div>
  );
}

export async function getServerSideProps() {
  try {
    // use ip address due to DNS lookup issue
    // https://github.com/nodejs/node/issues/40702#issuecomment-958143154
    const res = await fetch("http://bondanalyzer.dynv6.net/api/v1/signal");
    const apiData: StockSignal[] | undefined = await res.json();
    return { props: { apiData } };
  } catch (Error) {
    console.log(Error);
    const apiData: StockSignal[] = [];
    return { props: { apiData } };
  }
}
