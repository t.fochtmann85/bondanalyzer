import { ChangeEvent, useMemo, useState } from "react";
import { FilterValue, Row, useAsyncDebounce } from "react-table";
import "regenerator-runtime"; // due to a bug in react-table

// Define a default UI for filtering
export function DefaultColumnFilter<T extends Record<string, unknown>>({
  column: { filterValue, preFilteredRows, setFilter },
}: {
  column: {
    filterValue: FilterValue;
    preFilteredRows: Array<Row>;
    setFilter: (updater: FilterValue) => void;
  };
}) {
  const count = preFilteredRows.length;

  return (
    <input
      className="input is-small"
      type="text"
      value={filterValue || ""}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
    />
  );
}

// This is a custom filter UI for selecting
// a unique option from a list
export function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}: {
  column: {
    filterValue: FilterValue;
    preFilteredRows: Array<Row>;
    setFilter: (updater: FilterValue) => void;
    id: string;
  };
}) {
  // Calculate the options for filtering
  // using the preFilteredRows
  const options = useMemo(() => {
    const options = new Set<string>();
    preFilteredRows.forEach((row) => {
      if (row.values[id] != null) {
        options.add(row.values[id]);
      }
    });
    return [...options.values()];
  }, [id, preFilteredRows]);

  // Render a multi-select box
  //   if (options.length == 0) {
  //     return <span className="control">No values!</span>;
  //   }

  return (
    <div className="select is-small">
      <select
        value={filterValue}
        onChange={(e) => {
          setFilter(e.target.value || undefined);
        }}
      >
        <option value="">All</option>
        {options.map((option, i) => (
          <option key={i} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
}

// This is a custom UI for our 'between' or number range
// filter. It uses two number boxes and filters rows to
// ones that have values between the two
export function NumberRangeColumnFilter({
  column: { filterValue = [], preFilteredRows, setFilter, id },
}: {
  column: {
    filterValue: FilterValue;
    preFilteredRows: Array<Row>;
    setFilter: (updater: FilterValue) => void;
    id: string;
  };
}) {
  const [min, max] = useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    preFilteredRows.forEach((row) => {
      min = Math.min(row.values[id], min);
      max = Math.max(row.values[id], max);
    });
    return [min, max];
  }, [id, preFilteredRows]);

  return (
    <div className="columns is-vcentered is-centered">
      <div className="column is-half">
        <input
          value={filterValue[0] || ""}
          type="number"
          onChange={(e) => {
            const val = e.target.value;
            setFilter((old = []) => [
              val ? parseFloat(val) : undefined,
              old[1],
            ]);
          }}
          placeholder={`Min (${min})`}
          className="input is-small"
        />
      </div>
      <div className="column is-half">
        <input
          value={filterValue[1] || ""}
          type="number"
          onChange={(e) => {
            const val = e.target.value;
            setFilter((old = []) => [
              old[0],
              val ? parseFloat(val) : undefined,
            ]);
          }}
          placeholder={`Max (${max})`}
          className="input is-small"
        />
      </div>
    </div>
  );
}

// Define a default UI for filtering
export function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}: {
  preGlobalFilteredRows: Array<Row>;
  globalFilter: string;
  setGlobalFilter: CallableFunction;
}) {
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  const handleGlobalFilterChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
    onChange(e.target.value);
  };

  return (
    <span>
      Search:
      <input
        className="ml-2 m-1"
        type="text"
        value={value || ""}
        onChange={handleGlobalFilterChange}
        placeholder={`${count} records...`}
        style={{
          fontSize: "1.1rem",
          border: "0",
        }}
      />
    </span>
  );
}
