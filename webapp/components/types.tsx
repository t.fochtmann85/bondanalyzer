export type Stock = {
  inner: boolean;
  original: boolean;
  name: string;
  currency: string;
  ticker: string;
  isin: string;
  sector: string;
  id: number;
  market_cap: number;
  geopac: number;
  gain_consistency: number;
  loss_ratio: number;
  latest_price: number;
  latest_ma_200: number;
  months: number;
  score: number;
  trend: number;
  ath: number;
  ath_diff: number;
};

export type Signal = {
  date: string;
  signal_type: number;
  id: number;
  stock_id: number;
};

export type StockSignal = {
  stock: Stock;
  signal: Signal;
};

export type FrontendSignal = {
  date: string;
  signal_type: string;
};

export type FrontendStock = {
  original: boolean;
  name: string;
  currency: string;
  ticker: string;
  isin: string;
  sector: string;
  market_cap: number;
  geopac: number;
  gain_consistency: number;
  loss_ratio: number;
  latest_price: number;
  latest_ma_200: number;
  months: number;
  score: number;
  trend: number;
  ath: number;
  ath_diff: number;
};

export type FrontendStockSignal = FrontendStock & FrontendSignal;

export type SearchResultSpecifics = {
  count: number;
  sectors: Array<string>;
  market_cap_min: number;
  market_cap_max: number;
  months_min: number;
  months_max: number;
  score_min: number;
  score_max: number;
  geopac_min: number;
  geopac_max: number;
  gain_consistency_min: number;
  gain_consistency_max: number;
  loss_ratio_min: number;
  loss_ratio_max: number;
  trend_min: number;
  trend_max: number;
};

export type SearchFilter = {
  inner: boolean;
  sector?: string;
  market_cap_min?: number;
  market_cap_max?: number;
  months_min?: number;
  months_max?: number;
  score_min?: number;
  score_max?: number;
  geopac_min?: number;
  geopac_max?: number;
  gain_consistency_min?: number;
  gain_consistency_max?: number;
  loss_ratio_min?: number;
  loss_ratio_max?: number;
  trend_min?: number;
  trend_max?: number;
};

export type ForeignStockData = {
  rawStocks: Array<Stock> | undefined;
  isLoading: boolean;
  isError: Error | undefined;
};
