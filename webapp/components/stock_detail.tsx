import { MouseEvent } from "react";
import { StockSignal } from "webapp/components/types";

export function StockDetail({ stockSignal }: { stockSignal: StockSignal }) {
  const stock = stockSignal.stock;
  const currency = stock ? { EUR: "€" }[stock.currency] : null;

  const closeModal = (e: MouseEvent) => {
    closeClosestModal(e.target as HTMLElement);
  };

  const closeClosestModal = (elem: HTMLElement) => {
    elem.closest(".modal")?.classList.remove("is-active");
  };

  const escapedTicker = stock.ticker.replace(/\W/, "-");

  return (
    <div className="modal" id={"stock_" + escapedTicker}>
      <div
        className="modal-background"
        onClick={(e) => {
          closeClosestModal(e.target as HTMLElement);
        }}
      ></div>

      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title has-text-primary">{stock.name}</p>
          <button
            className="delete"
            aria-label="close"
            onClick={(e) => {
              closeModal(e as MouseEvent);
            }}
          ></button>
        </header>
        <div className="modal-card-body">
          <div className="columns is-fullwidth">
            <div className="column is-one-quarter">
              <article className="tile is-child notification is-warning is-vertical">
                <p className="subtitle">Market Capitalization</p>
                <p className="title">
                  {stock ? toThousandSeparator(stock.market_cap) : null}{" "}
                  {currency}
                </p>
              </article>
            </div>
          </div>
        </div>
      </div>
      <button
        className="modal-close is-large"
        aria-label="close"
        onClick={(e) => {
          closeModal(e as MouseEvent);
        }}
      ></button>
    </div>
  );
}

function toThousandSeparator(x: number) {
  const xAsString = x.toString();
  let newX = "";
  for (let i = 0; i < xAsString.length; i++) {
    if (i % 3 == 0 && i > 0) newX = "." + newX;
    newX = xAsString.charAt(xAsString.length - 1 - i) + newX;
  }
  return newX;
}
