import {
  FrontendSignal,
  FrontendStock,
  FrontendStockSignal,
  Signal,
  Stock,
  StockSignal,
} from "./types";

export function getFrontendDataFromRawStock(stock: Stock): FrontendStock {
  return {
    ...stock,
    market_cap: Math.round((stock.market_cap / 10 ** 9) * 100) / 100,
    geopac: Math.round(stock.geopac * 100),
    gain_consistency: Math.round(stock.gain_consistency * 100),
    loss_ratio: Math.round(stock.loss_ratio * 10) / 10,
    latest_price: Math.round(stock.latest_price * 100) / 100,
    latest_ma_200: Math.round(stock.latest_ma_200 * 100) / 100,
    trend: Math.round(stock.trend * 10) / 10,
    ath: Math.round(stock.ath * 100) / 100,
    ath_diff: Math.round(stock.ath_diff * 10) / 10,
  };
}

export function getFrontendDataFromRawSignal(signal: Signal): FrontendSignal {
  const signalTypeMap: { [key: number]: string } = {
    1: "buy",
    2: "hold",
    3: "buy again",
    4: "trend reversal",
  };

  return {
    ...signal,
    signal_type: signalTypeMap[signal.signal_type],
  };
}

export function getFrontendDataFromRawStockSignal(
  stockSignal: StockSignal
): FrontendStockSignal {
  return {
    ...getFrontendDataFromRawStock(stockSignal.stock),
    ...getFrontendDataFromRawSignal(stockSignal.signal),
  };
}
