import Link from "next/link";
import { Cell, Column } from "react-table";
import { NumberRangeColumnFilter, SelectColumnFilter } from "./filters";

type ExtendedColumn = Column & {
  label?: string;
};

const truncate = 20;
const colStockChampion: ExtendedColumn = {
  Header: () => (
    <abbr title="Champion">
      <i className="fa fa-trophy" aria-hidden="true" />
    </abbr>
  ),
  accessor: "original",
  Cell: ({ cell: { value } }: { cell: { value: boolean } }) =>
    value ? <i className="fa fa-star" aria-hidden="true" /> : null,
  disableSortBy: true,
};
const colStockName: ExtendedColumn = {
  Header: "Name",
  accessor: "name",
  Cell: ({ cell: { value } }: { cell: { value: string } }) => (
    <div>
      <abbr title={value} hidden={value.length > truncate ? false : true}>
        {value.length > truncate ? value.slice(0, truncate - 1) + "..." : value}
      </abbr>
      <span hidden={value.length > truncate ? true : false}>{value}</span>
    </div>
  ),
};
const colStockTicker: ExtendedColumn = {
  Header: "Ticker",
  accessor: "ticker",
  Cell: ({ cell: { value } }: { cell: { value: string } }) => (
    <Link
      className="button is-small is-responsive is-primary is-fullwidth"
      href={"https://de.finance.yahoo.com/quote/".concat(value)}
    >
      <span className="mr-2">
        <i className="fa fa-link" />
      </span>
      <span>{value}</span>
    </Link>
  ),
};
const colStockSector: ExtendedColumn = {
  Header: "Sector",
  accessor: "sector",
  Filter: SelectColumnFilter,
  filter: "includes",
};
const colStockIsin: ExtendedColumn = {
  Header: "ISIN",
  accessor: "isin",
  Cell: ({ cell: { value } }: { cell: Cell }) => {
    return value.slice(0, 12);
  },
};
const colStockMCap: ExtendedColumn = {
  Header: () => <abbr title="Market capitalization (in billion)">MCap</abbr>,
  label: "Market capitalization",
  accessor: "market_cap",
  sortType: "basic",
  sortDescFirst: true,
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockMonths: ExtendedColumn = {
  Header: "Months",
  accessor: "months",
  sortType: "basic",
  sortDescFirst: true,
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockScore: ExtendedColumn = {
  Header: "Score",
  accessor: "score",
  sortType: "basic",
  sortDescFirst: true,
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockGeoPac: ExtendedColumn = {
  Header: () => <abbr title="geoPAK">GP %</abbr>,
  label: "geoPAK %",
  accessor: "geopac",
  sortType: "basic",
  sortDescFirst: true,
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockGainCons: ExtendedColumn = {
  Header: () => <abbr title="Gain consistency">GC %</abbr>,
  label: "Gain consistency %",
  accessor: "gain_consistency",
  sortType: "basic",
  sortDescFirst: true,
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockLossRatio: ExtendedColumn = {
  Header: () => <abbr title="Loss ratio">LR</abbr>,
  label: "Loss ratio",
  accessor: "loss_ratio",
  sortType: "basic",
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockPrice: ExtendedColumn = {
  Header: "Price",
  accessor: "latest_price",
  sortType: "basic",
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockAth: ExtendedColumn = {
  Header: () => <abbr title="All Time High">ATH</abbr>,
  label: "all time high",
  accessor: "ath",
  sortType: "basic",
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockAthDiff: ExtendedColumn = {
  Header: "ATH %",
  accessor: "ath_diff",
  sortType: "basic",
  Cell: ({ cell: { value } }: { cell: { value: number } }) => (
    <div className={value < 0 ? "has-text-danger" : "has-text-success"}>
      <span className="mr-2">{value}</span>
      <i className={value < 0 ? "fa fa-arrow-down" : "fa fa-arrow-up"} />
    </div>
  ),
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockMa200: ExtendedColumn = {
  Header: () => <abbr title="200-day moving average">MA 200</abbr>,
  label: "200 day moving average",
  accessor: "latest_ma_200",
  sortType: "basic",
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colStockTrend: ExtendedColumn = {
  Header: "Trend %",
  accessor: "trend",
  sortType: "basic",
  Cell: ({ cell: { value } }: { cell: { value: number } }) => (
    <div className={value < 0 ? "has-text-danger" : "has-text-success"}>
      <span className="mr-2">{value}</span>
      <i className={value < 0 ? "fa fa-arrow-down" : "fa fa-arrow-up"} />
    </div>
  ),
  filter: "between",
  Filter: NumberRangeColumnFilter,
};
const colSignalType: ExtendedColumn = {
  Header: "Last signal",
  accessor: "signal_type",
  Cell: ({ cell: { value } }: { cell: { value: string } }) => (
    <div
      className={
        value === "trend reversal"
          ? "has-text-danger"
          : value === "buy"
          ? "has-text-success"
          : ""
      }
    >
      {value}
    </div>
  ),
};
const colSignalDate: ExtendedColumn = {
  Header: "... from",
  accessor: "date",
  sortType: (a, b) => {
    return new Date(b.values.date).getTime() - new Date(a.values.date).getTime();
  }

};
export const tableHeaderStocks: Array<ExtendedColumn> = [
  colStockChampion,
  colStockName,
  colStockTicker,
  colStockSector,
  colStockIsin,
  colStockMCap,
  colStockMonths,
  colStockScore,
  colStockGeoPac,
  colStockGainCons,
  colStockLossRatio,
  colStockPrice,
  colStockAth,
  colStockAthDiff,
  colStockMa200,
  colStockTrend,
  colSignalType,
  colSignalDate,
];

export const tableHeaderForeignStocks: Array<ExtendedColumn> = [
  colStockName,
  colStockTicker,
  colStockSector,
  colStockIsin,
  colStockMCap,
  colStockMonths,
  colStockScore,
  colStockGeoPac,
  colStockGainCons,
  colStockLossRatio,
  colStockPrice,
  colStockAth,
  colStockAthDiff,
  colStockMa200,
  colStockTrend,
];
