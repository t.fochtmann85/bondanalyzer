import { MouseEvent } from "react";
import { Column, useSortBy, useTable } from "react-table";

export function StocksTable({
  columns,
  data,
}: {
  columns: Column[];
  data: any[];
}) {
  // Use the state and functions returned from useTable to build your UI
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable(
      {
        columns,
        data,
      },
      useSortBy
    );

  const onRowClick = (row: any, e: MouseEvent) => {
    const newTicker = row.original.ticker.replace(/\W/, "-");
    let rowNode = e.target as HTMLElement;
    const tableNode = rowNode.closest("#stocks-table");
    (
      tableNode?.parentNode?.querySelectorAll(".modal#stock_" + newTicker) || []
    ).forEach((value) => {
      value.classList.add("is-active");
    });
  };

  // Render the UI for your table
  return (
    <table
      id="stocks-table"
      {...getTableProps()}
      className="table is-striped is-hoverable is-fullwidth is-narrow"
    >
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(
              (
                column: any // cast to any to avoid TypescriptExceptions
              ) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  <span
                    hidden={column.disableSortBy ? true : false}
                    className="mr-2"
                  >
                    <i
                      className={
                        column.isSorted
                          ? column.isSortedDesc
                            ? "fa fa-sort-desc"
                            : "fa fa-sort-asc"
                          : "fa fa-sort"
                      }
                      aria-hidden="true"
                    />
                  </span>
                  {column.render("Header")}
                </th>
              )
            )}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr
              {...row.getRowProps()}
              onClick={(e) => onRowClick(row, e as MouseEvent)}
            >
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
