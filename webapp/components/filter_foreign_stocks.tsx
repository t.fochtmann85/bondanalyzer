import {
  ChangeEvent,
  ChangeEventHandler,
  FormEvent,
  MouseEvent,
  useState,
} from "react";
import { SearchFilter, SearchResultSpecifics } from "./types";

export function BaseFilterSettings({
  initSearchResultSpecs,
  searchFilter,
  setSearchSpecifics,
}: {
  initSearchResultSpecs: SearchResultSpecifics;
  searchFilter: SearchFilter;
  setSearchSpecifics: CallableFunction;
}) {
  const [searchSpecificsLocal, setSearchSpecificsLocal] =
    useState(searchFilter);

  const handleGenericChange = (
    e: ChangeEvent<HTMLInputElement>,
    parseFunc: CallableFunction,
    factor: number = 1
  ) => {
    const propName = e.target.name;
    const rawValue = e.target.value.trim();
    const value = parseFunc(rawValue);
    if (!Number.isNaN(value)) {
      setSearchSpecificsLocal({
        ...searchSpecificsLocal,
        [propName]: value * factor,
      });
    } else {
      delete searchSpecificsLocal[propName as keyof SearchFilter];
      setSearchSpecificsLocal(searchSpecificsLocal);
    }
  };

  const handleSector = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    if (value) {
      setSearchSpecificsLocal({
        ...searchSpecificsLocal,
        [e.target.name]: value,
      });
    } else {
      setSearchSpecificsLocal({
        ...searchSpecificsLocal,
        [e.target.name]: null,
      });
    }
  };

  const applyFilterSettings = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setSearchSpecifics({
      ...searchSpecificsLocal,
    });
    closeClosestModal(e.target as HTMLElement);
  };

  const openFilterModal = (e: MouseEvent) => {
    let button = e.target as HTMLInputElement;
    (button.parentNode?.parentNode?.querySelectorAll(".modal") || []).forEach((value) => {
      value.classList.add("is-active");
    });
  };

  const closeFilterModal = (e: MouseEvent) => {
    closeClosestModal(e.target as HTMLElement);
  };

  const closeClosestModal = (elem: HTMLElement) => {
    elem.closest(".modal")?.classList.remove("is-active");
  };

  return (
    <div>
      <button
        id="filter-button"
        className="button is-primary is-outlined"
        data-target="foreign-filter-settings"
        onClick={(e) => {
          openFilterModal(e as MouseEvent);
        }}
      >
        <span className="icon">
          <i className="fa fa-filter"></i>
        </span>
        <span>Filter</span>
      </button>
      <div className="modal" id="foreign-filter-settings">
        <div
          className="modal-background"
          onClick={(e) => {
            closeClosestModal(e.target as HTMLElement);
          }}
        ></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title has-text-primary">Filter settings</p>
            <button
              className="delete"
              aria-label="close"
              onClick={(e) => {
                closeFilterModal(e as MouseEvent);
              }}
            ></button>
          </header>
          <div className="modal-card-body">
            <form onSubmit={applyFilterSettings}>
              <div className="columns">
                <div className="column is-one-quarter">
                  <FromToComponent
                    label="Market Capitalization (Billion)"
                    name="market_cap"
                    min_value={initSearchResultSpecs.market_cap_min}
                    max_value={initSearchResultSpecs.market_cap_max}
                    handleChange={(e) => {
                      handleGenericChange(
                        e as ChangeEvent<HTMLInputElement>,
                        parseFloat,
                        1e9
                      );
                    }}
                    step={0.01}
                    min_constraint={0}
                    factor={1e-9}
                    decimals={2}
                  />
                  <Sector
                    sectors={initSearchResultSpecs.sectors}
                    handleSector={handleSector}
                  />
                </div>
                <div className="column is-one-quarter">
                  <FromToComponent
                    label="Score"
                    name="score"
                    min_value={initSearchResultSpecs.score_min}
                    max_value={initSearchResultSpecs.score_max}
                    handleChange={(e) => {
                      handleGenericChange(
                        e as ChangeEvent<HTMLInputElement>,
                        parseInt
                      );
                    }}
                  />
                  <FromToComponent
                    label="geoPAK %"
                    name="geopac"
                    min_value={initSearchResultSpecs.geopac_min}
                    max_value={initSearchResultSpecs.geopac_max}
                    handleChange={(e) => {
                      handleGenericChange(
                        e as ChangeEvent<HTMLInputElement>,
                        parseFloat,
                        1e-2
                      );
                    }}
                    step={0.1}
                    min_constraint={0}
                    factor={1e2}
                    decimals={1}
                  />
                </div>
                <div className="column is-one-quarter">
                  <FromToComponent
                    label="Gain consistency %"
                    name="gain_consistency"
                    min_value={initSearchResultSpecs.gain_consistency_min}
                    max_value={initSearchResultSpecs.gain_consistency_max}
                    handleChange={(e) => {
                      handleGenericChange(
                        e as ChangeEvent<HTMLInputElement>,
                        parseFloat,
                        1e-2
                      );
                    }}
                    step={0.1}
                    min_constraint={0}
                    max_constraint={100}
                    factor={1e2}
                    decimals={1}
                  />
                  <FromToComponent
                    label="Loss Ratio"
                    name="loss_ratio"
                    min_value={initSearchResultSpecs.loss_ratio_min}
                    max_value={initSearchResultSpecs.loss_ratio_max}
                    handleChange={(e) => {
                      handleGenericChange(
                        e as ChangeEvent<HTMLInputElement>,
                        parseFloat
                      );
                    }}
                    step={0.1}
                    min_constraint={0}
                    decimals={1}
                  />
                </div>
                <div className="column is-one-quarter">
                  <FromToComponent
                    label="Trend"
                    name="trend"
                    min_value={initSearchResultSpecs.trend_min}
                    max_value={initSearchResultSpecs.trend_max}
                    handleChange={(e) => {
                      handleGenericChange(
                        e as ChangeEvent<HTMLInputElement>,
                        parseFloat
                      );
                    }}
                    step={0.1}
                    decimals={1}
                  />
                  <FromToComponent
                    label="Months at exchange"
                    name="months"
                    min_value={initSearchResultSpecs.months_min}
                    min_constraint={0}
                    max_constraint={initSearchResultSpecs.months_max}
                    max_value={initSearchResultSpecs.months_max}
                    handleChange={(e) => {
                      handleGenericChange(
                        e as ChangeEvent<HTMLInputElement>,
                        parseInt
                      );
                    }}
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button
                    className="button is-primary is-fullwidth"
                    type="submit"
                  >
                    Filter
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <button className="modal-close is-large" aria-label="close"></button>
      </div>
    </div>
  );
}

function round(value: number, factor: number = 1, decimals: number = 0) {
  const rounded = Math.round(value * factor * 10 ** decimals) / 10 ** decimals;
  return rounded;
}

function Sector({
  sectors,
  handleSector,
}: {
  sectors: string[];
  handleSector: ChangeEventHandler;
}) {
  return (
    <fieldset className="field boxed">
      <legend className="label has-text-white">Sector</legend>
      <div className="control is-expanded">
        <div className="select is-fullwidth">
          <select name="sector" onChange={handleSector}>
            <option value="">All</option>
            {sectors.map((option, i) => (
              <option key={i} value={option}>
                {option}
              </option>
            ))}
          </select>
        </div>
      </div>
    </fieldset>
  );
}

function FromToComponent({
  label,
  name,
  min_constraint,
  max_constraint,
  min_value,
  max_value,
  step = 1,
  factor = 1,
  decimals = 0,
  handleChange,
}: {
  label: string;
  name: string;
  min_constraint?: number | undefined;
  max_constraint?: number | undefined;
  min_value: number;
  max_value: number;
  step?: number | undefined;
  factor?: number;
  decimals?: number;
  handleChange: ChangeEventHandler;
}) {
  return (
    <fieldset className="field boxed">
      <legend className="label has-text-white">{label}</legend>
      <div className="control is-expanded">
        <div className="columns is-vcentered is-centered">
          <div className="column is-narrow is-half">
            <input
              type="number"
              placeholder={`Min (${round(min_value, factor, decimals)})`}
              className="input"
              name={name + "_min"}
              min={min_constraint}
              max={max_constraint}
              onChange={handleChange}
              step={step}
            />
          </div>
          <div className="column is-narrow is-half">
            <input
              type="number"
              placeholder={`Max (${round(max_value, factor, decimals)})`}
              className="input"
              name={name + "_max"}
              min={min_constraint}
              max={max_constraint}
              onChange={handleChange}
              step={step}
            />
          </div>
        </div>
      </div>
    </fieldset>
  );
}
