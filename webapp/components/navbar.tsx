import Link from "next/link";
import { MouseEvent } from "react";

export default function Navbar() {
  const openFilterModal = (e: MouseEvent) => {
    const elem = e.target as HTMLElement;
    const navbarBurger = elem.closest(".navbar-burger") as HTMLElement;
    const navbarMenu =
      navbarBurger.parentNode?.parentNode?.querySelector(".navbar-menu");
    navbarBurger.classList.toggle("is-active");
    navbarMenu?.classList.toggle("is-active");
  };

  return (
    <nav
      className="navbar is-black"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand">
        <a
          role="button"
          className="navbar-burger"
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
          onClick={(e) => {
            openFilterModal(e as MouseEvent);
          }}
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navbarBasicExample" className="navbar-menu">
        <div className="navbar-start">
          <Link className="navbar-item" href="/">
            Stocks
          </Link>

          <Link className="navbar-item" href="/foreign_stocks">
            Foreigns
          </Link>

          {/* <a className="navbar-item">Signals</a> */}
        </div>

        <div className="navbar-end"></div>
      </div>
    </nav>
  );
}
