import { useMemo } from "react";
import { useGlobalFilter, useSortBy, useTable } from "react-table";
import useSWR from "swr";
import { getFrontendDataFromRawStock } from "./converter";
import fetcher from "./fetcher";
import { GlobalFilter } from "./filters";
import { tableHeaderForeignStocks } from "./table_headers";
import { ForeignStockData, SearchFilter, Stock } from "./types";

export function ForeignStocksTable({
  searchSpecifics,
}: {
  searchSpecifics: SearchFilter;
}) {
  let notes = undefined;
  let stocks = new Array();

  // (1) create header memo
  const columns = useMemo(() => {
    return tableHeaderForeignStocks;
  }, []);

  // (2) get data from REST-API
  const { rawStocks, isLoading, isError } = GetForeignStocks(searchSpecifics);

  if (rawStocks) {
    // (2a) - everything worked fine
    stocks = rawStocks;
  }
  if (isError) {
    // (2b) - an error occurred
    notes = (
      <div className="notification is-danger">
        Sorry, didn&apos;t receive any stocks from REST-API 😥
        <br />
        Reason: {isError.message}
      </div>
    );
  } else if (isLoading) {
    // (2c) - it's currently loading
    notes = (
      <div>
        <div className="notification is-primary m-2">
          Loading foreign stocks ...
        </div>
        <progress className="progress is-primary m-2" />
      </div>
    );
  }

  const data = useMemo(
    () => stocks.map((stock) => getFrontendDataFromRawStock(stock)),
    [isLoading]
  );

  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    visibleColumns,
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy
  );

  // Render the UI for your table
  return (
    <>
      <table
        id="foreign-stocks-table"
        {...getTableProps()}
        className="table is-striped is-hoverable is-fullwidth is-narrow"
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(
                (
                  column: any // cast to any to avoid TypescriptExceptions
                ) => (
                  <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {/* Add a sort direction indicator */}
                    <span
                      hidden={column.disableSortBy ? true : false}
                      className="mr-2"
                    >
                      <i
                        className={
                          column.isSorted
                            ? column.isSortedDesc
                              ? "fa fa-sort-desc"
                              : "fa fa-sort-asc"
                            : "fa fa-sort"
                        }
                        aria-hidden="true"
                      />
                    </span>
                    {column.render("Header")}
                  </th>
                )
              )}
            </tr>
          ))}
          <tr>
            <th
              colSpan={visibleColumns.length}
              style={{
                textAlign: "left",
              }}
            >
              <GlobalFilter
                preGlobalFilteredRows={preGlobalFilteredRows}
                globalFilter={state.globalFilter}
                setGlobalFilter={setGlobalFilter}
              />
            </th>
          </tr>
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      {notes}
    </>
  );
}

function GetForeignStocks(searchSpecifics: SearchFilter): ForeignStockData {
  const { data, error, isLoading } = useSWR<Stock[], Error>(
    {
      input: "http://bondanalyzer.dynv6.net/api/v1/stock/search",
      init: {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(searchSpecifics),
      },
    },
    fetcher,
    {
      revalidateIfStale: false,
      revalidateOnFocus: false,
    }
  );
  return {
    rawStocks: data,
    isLoading,
    isError: error,
  };
}
